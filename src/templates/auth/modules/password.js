$(function() {
	var progress = $("#password-strength");
	var warning  = $("#password-warning");
	var feedback = $("#password-feedback");

	var password1 = $("#password1");
	var password2 = $("#password2");

	var form = password1.parents("form");
	var submit = form.find(":submit");

	// Fetch words that are common to the user
	var user_inputs = password1.data("user-input").split(" ");

	var quality;

	password1.keyup(function(event) {
		form.trigger("change");
	});

	password2.keyup(function(event) {
		form.trigger("change");
	});

	form.on("change", function() {
		submit.prop("disabled", true);

		var val1 = password1.val();
		var val2 = password2.val();

		if (val1) {
			// Estimate password quality
			quality = zxcvbn(val1, user_inputs);

			// Convert score into percentage
			var percentage = (quality.score + 1) * 20;

			// Set progress bar width
			progress.val(percentage);

			// Clear all previous backgrounds
			progress.removeClass([
				"is-success", "is-warning", "is-danger"
			]);

			// Make progress bar show in the right colour
			switch (quality.score) {
				case 0:
				case 1:
				case 2:
					progress.addClass("is-danger");
					break;

				case 3:
					progress.addClass("is-warning");
					break;

				case 4:
					progress.addClass("is-success");
					break;
			}

			// Show any feedback
			feedback.empty();

			if (quality.feedback) {
				if (val2 && (val1 !== val2)) {
					feedback.append("<li>{{ _("Passwords do not match") }}</li>");
				}

				if (quality.feedback.warning) {
					feedback.append("<li>" + quality.feedback.warning + "</li>");
				}

				$.each(quality.feedback.suggestions, function (i, suggestion) {
					feedback.append("<li>" + suggestion + "</li>");
				});
			}
		} else {
			progress.val(0);

			// Clear all feedback
			feedback.empty();
		}

		// We cannot submit the form when password2 is empty
		if (!val2)
			return;

		if (!quality || quality.score < 3)
			return;

		// They match, so we can enable the submit button
		submit.prop("disabled", false);
	});
});
