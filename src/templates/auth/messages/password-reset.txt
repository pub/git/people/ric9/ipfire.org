From: IPFire Project <no-reply@ipfire.org>
To: {{ account.email_to }}
Subject: {{ _("Please Reset Your Password") }}
X-Auto-Response-Suppress: OOF

{{ _("Hello %s!") % account.first_name }}

{{ _("You, or somebody else on your behalf, has requested to change your password.") }} {{ _("If this was not you, please notify a team member.") }}

{{ _("To reset your password, please click on this link:") }}

  https://www.ipfire.org/password-reset/{{ account.uid }}/{{ reset_code }}
