From: IPFire Project <no-reply@ipfire.org>
To: {{ account.email_to }}
Subject: {{ post.title }}
Auto-Submitted: auto-generated
Precedence: bulk
X-Auto-Response-Suppress: OOF

{{ _("Hello %s,") % account.first_name }}

{{ _("there is a new post from %s on the IPFire Blog:") % post.author }}

  {{ post.title }}

{% if post.excerpt %}{{ "\n".join(("  %s" % l for l in post.excerpt.splitlines())) }}{% end %}

{{ _("Click here to read more:") }}

  https://www.ipfire.org/blog/{{ post.slug }}?utm_medium=email&utm_source=blog-announcement
