#!/usr/bin/python3

import datetime
import tornado.web

from . import base
from . import ui_modules

class IndexHandler(base.BaseHandler):
	@tornado.web.authenticated
	def get(self):
		# Check access permissions
		if not self.current_user.is_admin():
			raise tornado.web.HTTPError(403)

		self.render("analytics/index.html")


class DocsHandler(base.BaseHandler):
	@tornado.web.authenticated
	def get(self):
		# Check access permissions
		if not self.current_user.is_admin():
			raise tornado.web.HTTPError(403)

		# Most Popular Pages
		popular_pages = self.backend.analytics.get_most_popular_docs_pages(
			self.request.host, since=datetime.timedelta(hours=24 * 365), limit=50)

		# Popular Searches
		popular_searches = self.backend.analytics.get_search_queries(
			self.request.host, "/docs/search", limit=25)

		self.render("analytics/docs.html",
			popular_pages=popular_pages, popular_searches=popular_searches)


class SummaryModule(ui_modules.UIModule):
	def render(self, host=None, uri=None):
		if host is None:
			host = self.request.host

		if uri is None:
			uri = self.request.path

		# Fetch the total number of page views
		total_page_views = self.backend.analytics.get_page_views(host, uri)

		# Fetch the total number of page views in the last 24h
		total_page_views_24h = self.backend.analytics.get_page_views(host, uri,
			since=datetime.timedelta(hours=24))

		return self.render_string("analytics/modules/summary.html", host=host, uri=uri,
			total_page_views=total_page_views, total_page_views_24h=total_page_views_24h)
