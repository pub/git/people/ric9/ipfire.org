#!/usr/bin/python3

import difflib
import tornado.web

from . import base
from . import ui_modules

class PageHandler(base.AnalyticsMixin, base.BaseHandler):
	@property
	def action(self):
		return self.get_argument("action", None)

	def write_error(self, status_code, **kwargs):
		# Render a custom page for 404
		if status_code == 404:
			self.render("docs/404.html", **kwargs)
			return

		# Otherwise raise this to one layer above
		super().write_error(status_code, **kwargs)

	@tornado.web.removeslash
	def get(self, path):
		if path is None:
			path = "/"

		# Check permissions
		if not self.backend.wiki.check_acl(path, self.current_user):
			raise tornado.web.HTTPError(403, "Access to %s not allowed for %s" % (path, self.current_user))

		# Check if we are asked to render a certain revision
		revision = self.get_argument("revision", None)

		# Fetch the wiki page
		page = self.backend.wiki.get_page(path, revision=revision)

		# Diff
		if self.action == "diff":
			# Get both revisions
			a = self.get_argument("a")
			b = self.get_argument("b")

			# Fetch both versions of the page
			a = self.backend.wiki.get_page(path, revision=a)
			b = self.backend.wiki.get_page(path, revision=b)
			if not a or not b:
				raise tornado.web.HTTPError(404)

			# Cannot render a diff for the identical page
			if a == b:
				raise tornado.web.HTTPError(400)

			# Make sure that b is newer than a
			if a > b:
				a, b = b, a

			self.render("docs/diff.html", page=page, a=a, b=b)
			return

		# Restore
		elif self.action == "restore":
			self.render("docs/confirm-restore.html", page=page)
			return

		# Revisions
		elif self.action == "revisions":
			self.render("docs/revisions.html", page=page)
			return

		# If the page does not exist, we send 404
		if not page or page.was_deleted():
			# Handle /start links which were in the format of DokuWiki
			if path.endswith("/start"):
				# Strip /start from path
				path = path[:-6] or "/"

				# Redirect user to page if it exists
				page = self.backend.wiki.page_exists(path)
				if page:
					self.redirect(path)

			raise tornado.web.HTTPError(404)

		# Fetch the latest revision
		latest_revision = page.get_latest_revision()

		# Render page
		self.render("docs/page.html", page=page, latest_revision=latest_revision)


class FilesHandler(base.BaseHandler):
	@tornado.web.authenticated
	def get(self, path):
		if path is None:
			path = "/"

		# Check permissions
		if not self.backend.wiki.check_acl(path, self.current_user):
			raise tornado.web.HTTPError(403, "Access to %s not allowed for %s" % (path, self.current_user))

		files = self.backend.wiki.get_files(path)

		self.render("docs/files/index.html", path=path, files=files)


class FileHandler(base.AnalyticsMixin, base.BaseHandler):
	@property
	def action(self):
		return self.get_argument("action", None)

	async def get(self, path):
		# Check permissions
		if not self.backend.wiki.check_acl(path, self.current_user):
			raise tornado.web.HTTPError(403, "Access to %s not allowed for %s" % (path, self.current_user))

		# Check if we are asked to render a certain revision
		revision = self.get_argument("revision", None)

		# Fetch the file
		file = self.backend.wiki.get_file_by_path(path, revision=revision)
		if not file:
			raise tornado.web.HTTPError(404, "Could not find %s" % path)

		# Render detail page
		if self.action == "detail":
			page = None

			for breadcrumb, title in self.backend.wiki.make_breadcrumbs(path):
				page = self.backend.wiki.get_page(breadcrumb)
				if page:
					break

			self.render("docs/files/detail.html", page=page, file=file)
			return

		# Get image size
		size = self.get_argument_int("s", None)

		# Check if image should be resized
		if size and file.is_bitmap_image():
			# Send WEBP if the browser supports
			if self.browser_accepts("image/webp"):
				format = "WEBP"

			# Fall back to the native format
			else:
				format = None

			blob = await file.get_thumbnail(size, format=format)
		else:
			blob = file.blob

		# Allow downstream to cache this for a year
		if revision:
			self.set_expires(31536000)

		# Send the payload
		self._deliver_file(blob, filename=file.filename)


class EditHandler(base.BaseHandler):
	@tornado.web.authenticated
	def get(self, path):
		if path is None:
			path = "/"

		# Check permissions
		if not self.backend.wiki.check_acl(path, self.current_user):
			raise tornado.web.HTTPError(403, "Access to %s not allowed for %s" % (path, self.current_user))

		# Fetch the wiki page
		page = self.backend.wiki.get_page(path)

		# Empty page if it was deleted
		if page and page.was_deleted():
			page = None

		# Render page
		self.render("docs/edit.html", page=page, path=path)

	@tornado.web.authenticated
	def post(self, path):
		if path is None:
			path = "/"

		# Check permissions
		if not self.backend.wiki.check_acl(path, self.current_user):
			raise tornado.web.HTTPError(403, "Access to %s not allowed for %s" % (path, self.current_user))

		content = self.get_argument("content", None)
		changes = self.get_argument("changes")

		# Create a new page in the database
		with self.db.transaction():
			page = self.backend.wiki.create_page(path,
				self.current_user, content, changes=changes, address=self.get_remote_ip())

			# Add user as a watcher if wanted
			watch = self.get_argument("watch", False)
			if watch:
				page.add_watcher(self.current_user)

		# Redirect back
		if page.was_deleted():
			self.redirect("/")
		else:
			self.redirect(page.url)

	def on_finish(self):
		"""
			Updates the search index after the page has been edited
		"""
		# This is being executed in the background and after
		# the response has been set to the client
		with self.db.transaction():
			self.backend.wiki.refresh()


class RenderHandler(base.BaseHandler):
	def check_xsrf_cookie(self):
		pass # disabled

	@tornado.web.authenticated
	@base.ratelimit(minutes=5, requests=180)
	def post(self, path):
		if path is None:
			path = "/"

		content = self.get_argument("content")

		# Render the content
		renderer = self.backend.wiki.render(path, content)

		self.finish(renderer.html)


class RestoreHandler(base.BaseHandler):
	@tornado.web.authenticated
	@base.ratelimit(minutes=60, requests=24)
	def post(self):
		path = self.get_argument("path")

		# Check permissions
		if not self.backend.wiki.check_acl(path, self.current_user):
			raise tornado.web.HTTPError(403, "Access to %s not allowed for %s" % (path, self.current_user))

		# Check if we are asked to render a certain revision
		revision = self.get_argument("revision", None)
		comment = self.get_argument("comment", None)

		# Fetch the wiki page
		page = self.backend.wiki.get_page(path, revision=revision)

		with self.db.transaction():
			page = page.restore(
				author=self.current_user,
				address=self.get_remote_ip(),
				comment=comment,
			)

		# Redirect back to page
		self.redirect(page.page)


class UploadHandler(base.BaseHandler):
	@tornado.web.authenticated
	@base.ratelimit(minutes=60, requests=24)
	def post(self):
		path = self.get_argument("path")

		# Check permissions
		if not self.backend.wiki.check_acl(path, self.current_user):
			raise tornado.web.HTTPError(403, "Access to %s not allowed for %s" % (path, self.current_user))

		try:
			filename, data, mimetype = self.get_file("file")

			# Use filename from request if any
			filename = self.get_argument("filename", filename)

			# XXX check valid mimetypes

			with self.db.transaction():
				file = self.backend.wiki.upload(path, filename, data,
					mimetype=mimetype, author=self.current_user,
					address=self.get_remote_ip())

		except TypeError as e:
			raise e

		self.redirect("/docs%s/_files" % path)


class WatchHandler(base.BaseHandler):
	@tornado.web.authenticated
	@base.ratelimit(minutes=60, requests=180)
	def get(self, path, action):
		if path is None:
			path = "/"

		page = self.backend.wiki.get_page(path)
		if not page:
			raise tornado.web.HTTPError(404, "Page does not exist: %s" % path)

		# Check permissions
		if not self.backend.wiki.check_acl(path, self.current_user):
			raise tornado.web.HTTPError(403, "Access to %s not allowed for %s" % (path, self.current_user))

		with self.db.transaction():
			if action == "watch":
				page.add_watcher(self.current_user)
			elif action == "unwatch":
				page.remove_watcher(self.current_user)

		# Redirect back to page
		self.redirect(page.url)


class DeleteFileHandler(base.BaseHandler):
	@tornado.web.authenticated
	def get(self, path):
		# Check permissions
		if not self.backend.wiki.check_acl(path, self.current_user):
			raise tornado.web.HTTPError(403, "Access to %s not allowed for %s" % (path, self.current_user))

		# Fetch the file
		file = self.backend.wiki.get_file_by_path(path)
		if not file:
			raise tornado.web.HTTPError(404, "Could not find %s" % path)

		# Check if this can be deleted
		if not file.can_be_deleted():
			raise tornado.web.HTTPError(400, "%s cannot be deleted" % file)

		self.render("docs/confirm-delete.html", file=file)

	@tornado.web.authenticated
	@base.ratelimit(minutes=60, requests=24)
	def post(self, path):
		# Check permissions
		if not self.backend.wiki.check_acl(path, self.current_user):
			raise tornado.web.HTTPError(403, "Access to %s not allowed for %s" % (path, self.current_user))

		# Fetch the file
		file = self.backend.wiki.get_file_by_path(path)
		if not file:
			raise tornado.web.HTTPError(404, "Could not find %s" % path)

		# Check if this can be deleted
		if not file.can_be_deleted():
			raise tornado.web.HTTPError(400, "%s cannot be deleted" % file)

		with self.db.transaction():
			file.delete(self.current_user)

		self.redirect("/docs%s/_files" % file.path)


class SearchHandler(base.AnalyticsMixin, base.BaseHandler):
	@base.ratelimit(minutes=5, requests=25)
	def get(self):
		q = self.get_argument("q")

		# Perform search
		with self.db.transaction():
			pages = self.backend.wiki.search(q, account=self.current_user, limit=50)

		self.render("docs/search-results.html", q=q, pages=pages)


class RecentChangesHandler(base.AnalyticsMixin, base.BaseHandler):
	def get(self):
		recent_changes = self.backend.wiki.get_recent_changes(self.current_user, limit=50)

		self.render("docs/recent-changes.html", recent_changes=recent_changes)


class TreeHandler(base.AnalyticsMixin, base.BaseHandler):
	def get(self):
		self.render("docs/tree.html", pages=self.backend.wiki)


class WatchlistHandler(base.AnalyticsMixin, base.BaseHandler):
	@tornado.web.authenticated
	def get(self):
		pages = self.backend.wiki.get_watchlist(self.current_user)

		self.render("docs/watchlist.html", pages=pages)


class ListModule(ui_modules.UIModule):
	def render(self, pages, link_revision=False, show_breadcrumbs=True,
			show_author=True, show_changes=False):
		return self.render_string("docs/modules/list.html", link_revision=link_revision,
			pages=pages, show_breadcrumbs=show_breadcrumbs,
			show_author=show_author, show_changes=show_changes)


class HeaderModule(ui_modules.UIModule):
	@property
	def page(self):
		"""
			Returns the path of the page (without any actions)
		"""
		path = self.request.path.removeprefix("/docs")

		return "/".join((p for p in path.split("/") if not p.startswith("_")))

	def render(self, suffix=None):
		_ = self.locale.translate

		breadcrumbs = self.backend.wiki.make_breadcrumbs(self.page)
		title = self.backend.wiki.get_page_title(self.page)

		if self.request.path.endswith("/_edit"):
			suffix = _("Edit")
		elif self.request.path.endswith("/_files"):
			suffix = _("Files")

		return self.render_string("docs/modules/header.html",
			breadcrumbs=breadcrumbs, page=self.page, page_title=title, suffix=suffix)


class DiffModule(ui_modules.UIModule):
	differ = difflib.Differ()

	def render(self, a, b):
		diff = self.differ.compare(
			a.markdown.splitlines(),
			b.markdown.splitlines(),
		)

		return self.render_string("docs/modules/diff.html", diff=diff)
