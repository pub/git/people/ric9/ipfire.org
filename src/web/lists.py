#!/usr/bin/python3

import tornado.web

from . import base

class IndexHandler(base.BaseHandler):
	@tornado.web.authenticated
	async def get(self):
		# Fetch all available lists
		lists = await self.backend.lists.get_lists()

		# Fetch all subscribed lists
		subscribed_lists = await self.current_user.get_lists()

		self.render("lists/index.html", lists=lists, subscribed_lists=subscribed_lists)
