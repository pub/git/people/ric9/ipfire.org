#!/usr/bin/python

import asyncio
import base64
import datetime
import dateutil.parser
import functools
import http.client
import ipaddress
import logging
import magic
import mimetypes
import time
import tornado.locale
import tornado.web

from ..decorators import *
from .. import util

# Setup logging
log = logging.getLogger(__name__)

class ratelimit(object):
	"""
		A decorator class which limits how often a function can be called
	"""
	def __init__(self, *, minutes, requests):
		self.minutes  = minutes
		self.requests = requests

	def __call__(self, method):
		@functools.wraps(method)
		async def wrapper(handler, *args, **kwargs):
			# Pass the request to the rate limiter and get a request object
			req = handler.backend.ratelimiter.handle_request(handler.request,
				handler, minutes=self.minutes, limit=self.requests)

			# If the rate limit has been reached, we won't allow
			# processing the request and therefore send HTTP error code 429.
			if await req.is_ratelimited():
				raise tornado.web.HTTPError(429, "Rate limit exceeded")

			# Call the wrapped method
			result = method(handler, *args, **kwargs)

			# Await it if it is a coroutine
			if asyncio.iscoroutine(result):
				return await result

			# Return the result
			return result

		return wrapper


class BaseHandler(tornado.web.RequestHandler):
	def prepare(self):
		# Mark this as private when someone is logged in
		if self.current_user:
			self.set_header("Cache-Control", "private")

		# Always send Vary: Cookie
		self.set_header("Vary", "Cookie")

	def set_expires(self, seconds):
		# For HTTP/1.1
		self.add_header("Cache-Control", "max-age=%s, must-revalidate" % seconds)

		# For HTTP/1.0
		expires = datetime.datetime.utcnow() + datetime.timedelta(seconds=seconds)
		self.set_header("Expires", expires)

	def write_error(self, status_code, **kwargs):
		# Translate code into message
		try:
			message = http.client.responses[status_code]
		except KeyError:
			message = None

		self.render("error.html", status_code=status_code, message=message, **kwargs)

	def browser_accepts(self, t):
		"""
			Checks if type is in Accept: header
		"""
		accepts = []

		for elem in self.request.headers.get("Accept", "").split(","):
			# Remove q=N
			type, delim, q = elem.partition(";")

			accepts.append(type)

		# Check if the filetype is in the list of accepted ones
		return t in accepts

	@property
	def hostname(self):
		# Return hostname in production
		if self.request.host.endswith("ipfire.org"):
			return self.request.host

		# Remove the development prefix
		subdomain, delimier, domain = self.request.host.partition(".")
		if subdomain:
			return "%s.ipfire.org" % subdomain

		# Return whatever it is
		return self.request.host

	def get_template_namespace(self):
		ns = tornado.web.RequestHandler.get_template_namespace(self)

		now = datetime.date.today()

		ns.update({
			"backend"     : self.backend,
			"debug"       : self.application.settings.get("debug", False),
			"format_size" : util.format_size,
			"format_time" : util.format_time,
			"hostname"    : self.hostname,
			"now"         : now,
			"q"           : None,
			"year"        : now.year,
		})

		return ns

	def get_remote_ip(self):
		# Fix for clients behind a proxy that sends "X-Forwarded-For".
		remote_ips = self.request.remote_ip.split(", ")

		for remote_ip in remote_ips:
			try:
				addr = ipaddress.ip_address(remote_ip)
			except ValueError:
				# Skip invalid IP addresses.
				continue

			# Check if the given IP address is from a
			# private network.
			if addr.is_private:
				continue

			return remote_ip

		# Return the last IP if nothing else worked
		return remote_ips.pop()

	@lazy_property
	def current_address(self):
		address = self.get_remote_ip()

		if address:
			return util.Address(self.backend, address)

	@lazy_property
	def current_country_code(self):
		if self.current_address:
			return self.current_address.country_code

	@property
	def user_agent(self):
		"""
			Returns the HTTP user agent
		"""
		return self.request.headers.get("User-Agent", None)

	@property
	def referrer(self):
		return self.request.headers.get("Referer", None)

	def _request_basic_authentication(self):
		"""
			Called to ask the client to perform HTTP Basic authentication
		"""
		# Ask for authentication
		self.set_status(401)

		# Say that we support Basic
		self.set_header("WWW-Authenticate", "Basic realm=Restricted")

		self.finish()

	def perform_basic_authentication(self):
		"""
			This handles HTTP Basic authentication.
		"""
		# Fetch credentials
		cred = self.request.headers.get("Authorization", None)
		if not cred:
			return self._request_basic_authentication()

		# No basic auth? We cannot handle that
		if not cred.startswith("Basic "):
			return self._request_basic_authentication()

		# Decode the credentials
		try:
			# Convert into bytes()
			cred = cred[6:].encode()

			# Decode base64
			cred = base64.b64decode(cred).decode()

			username, password = cred.split(":", 1)

		# Fail if any of those steps failed
		except:
			raise e
			raise tornado.web.HTTPError(400, "Authorization data was malformed")

		# Find the user in the database
		return self.backend.accounts.auth(username, password)

		# Log something
		if account:
			log.info("%s authenticated successfully using HTTP Basic authentication" % account.uid)
		else:
			log.warning("Could not authenticate %s" % username)

		return account

	def get_argument_int(self, *args, **kwargs):
		arg = self.get_argument(*args, **kwargs)

		if arg is None or arg == "":
			return

		try:
			return int(arg)
		except ValueError:
			raise tornado.web.HTTPError(400, "Could not convert integer: %s" % arg)

	def get_argument_float(self, *args, **kwargs):
		arg = self.get_argument(*args, **kwargs)

		if arg is None or arg == "":
			return

		try:
			return float(arg)
		except ValueError:
			raise tornado.web.HTTPError(400, "Could not convert float: %s" % arg)

	def get_argument_date(self, arg, *args, **kwargs):
		value = self.get_argument(arg, *args, **kwargs)
		if value is None:
			return

		try:
			return dateutil.parser.parse(value)
		except ValueError:
			raise tornado.web.HTTPError(400)

	def get_file(self, name):
		try:
			file = self.request.files[name][0]

			return file["filename"], file["body"], file["content_type"]
		except KeyError:
			return None

	# Initialize libmagic
	magic = magic.Magic(mime=True, uncompress=True)

	# File

	def _deliver_file(self, data, filename=None, prefix=None):
		# Guess content type
		mimetype = self.magic.from_buffer(data)

		# Send the mimetype
		self.set_header("Content-Type", mimetype or "application/octet-stream")

		# Fetch the file extension
		if not filename and prefix:
			ext = mimetypes.guess_extension(mimetype)

			# Compose a new filename
			filename = "%s%s" % (prefix, ext)

		# Set filename
		if filename:
			self.set_header("Content-Disposition", "inline; filename=\"%s\"" % filename)

		# Set size
		if data:
			self.set_header("Content-Length", len(data))

		# Deliver payload
		self.finish(data)

	# Login stuff

	def get_current_user(self):
		session_id = self.get_cookie("session_id")
		if not session_id:
			return

		# Get account from the session object
		account = self.backend.accounts.get_by_session(session_id, self.request.host)

		# If the account was not found or the session was not valid
		# any more, we will remove the cookie.
		if not account:
			self.clear_cookie("session_id")

		return account

	@property
	def backend(self):
		return self.application.backend

	@property
	def db(self):
		return self.backend.db

	@property
	def accounts(self):
		return self.backend.accounts

	@property
	def downloads(self):
		return self.backend.downloads

	@property
	def fireinfo(self):
		return self.backend.fireinfo

	@property
	def iuse(self):
		return self.backend.iuse

	@property
	def mirrors(self):
		return self.backend.mirrors

	@property
	def netboot(self):
		return self.backend.netboot

	@property
	def releases(self):
		return self.backend.releases


class AnalyticsMixin(object):
	def on_finish(self):
		"""
			Collect some data about this request
		"""
		# Log something
		log.debug("Analytics for %s:" % self)
		log.debug("  User-Agent: %s" % self.user_agent)
		log.debug("  Referrer  : %s" % self.referrer)

		# Do nothing if this requst should be ignored
		if self._ignore_analytics():
			return

		with self.db.transaction():
			# Log unique visits
			self.backend.analytics.log_unique_visit(
				address=self.current_address,
				referrer=self.referrer,
				country_code=self.current_country_code,
				user_agent=self.user_agent,
				host=self.request.host,
				uri=self.request.uri,

				# UTMs
				source=self.get_argument("utm_source", None),
				medium=self.get_argument("utm_medium", None),
				campaign=self.get_argument("utm_campaign", None),
				content=self.get_argument("utm_content", None),
				term=self.get_argument("utm_term", None),

				# Search queries
				q=self.get_argument("q", None),
			)

	def _ignore_analytics(self):
		"""
			Checks if this request should be ignored
		"""
		ignored_user_agents = (
			"LWP::Simple",
			"check_http",
		)

		# Only log GET requests
		if not self.request.method == "GET":
			return True

		# Ignore everything from matching user agents
		if self.user_agent:
			for ignored_user_agent in ignored_user_agents:
				if self.user_agent.startswith(ignored_user_agent):
					return True


class APIHandler(BaseHandler):
	def check_xsrf_cookie(self):
		"""
			Do nothing here, because we cannot verify the XSRF token
		"""
		pass

	def prepare(self):
		# Do not cache any API communication
		self.set_header("Cache-Control", "no-cache")


class NotFoundHandler(BaseHandler):
	def prepare(self):
		# Raises 404 as soon as it is called
		raise tornado.web.HTTPError(404)


class ErrorHandler(BaseHandler):
	"""
		Raises any error we want
	"""
	def get(self, code):
		try:
			code = int(code)
		except:
			raise tornado.web.HTTPError(400)

		raise tornado.web.HTTPError(code)
