#!/usr/bin/python3

import tornado.web

from .. import util

from . import base

class IndexHandler(base.AnalyticsMixin, base.BaseHandler):
	def get(self):
		self.render("location/index.html",
			address=self.current_address,
		)


class LookupHandler(base.AnalyticsMixin, base.BaseHandler):
	async def get(self, address):
		# Lookup address
		address = util.Address(self.backend, address)

		# Lookup blacklists
		blacklists = await address.get_blacklists()

		self.render("location/lookup.html", address=address, blacklists=blacklists)
