#!/usr/bin/python

import tornado.web

from .. import database

class UIModule(tornado.web.UIModule):
	@property
	def backend(self):
		return self.handler.backend


class IPFireLogoModule(UIModule):
	def render(self, suffix=None):
		return self.render_string("modules/ipfire-logo.html", suffix=suffix)


class MarkdownModule(UIModule):
	def render(self, markdown):
		return self.backend.blog._render_text(markdown)


class MapModule(UIModule):
	def render(self, search):
		return self.render_string("modules/map.html", search=search)

	def css_files(self):
		return (
			"css/leaflet.css",
			"css/Control.Geocoder.css",
		)

	def javascript_files(self):
		return (
			"js/leaflet.min.js",
			"js/Control.Geocoder.min.js",
			"js/maps.js",
		)


class ProgressBarModule(UIModule):
	def render(self, value, colour=None):
		value *= 100

		return self.render_string("modules/progress-bar.html",
			colour=colour, value=value)
