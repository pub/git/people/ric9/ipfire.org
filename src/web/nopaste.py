#!/usr/bin/python

import tornado.web

from . import base
from . import ui_modules

class CreateHandler(base.AnalyticsMixin, base.BaseHandler):
	@tornado.web.authenticated
	def get(self):
		self.render("nopaste/create.html")

	@tornado.web.authenticated
	@base.ratelimit(minutes=15, requests=5)
	def post(self):
		subject = self.get_argument("subject", None)
		content = self.get_argument("content")

		# Fetch expires time
		expires = self.get_argument_int("expires", "0")

		with self.db.transaction():
			paste = self.backend.nopaste.create(content, subject=subject, expires=expires,
				account=self.current_user, address=self.get_remote_ip())

		# Redirect to the paste
		return self.redirect("/view/%s" % paste.uuid)

	# cURL Interface

	def get_current_user(self):
		if self.request.method == "PUT":
			return self.perform_basic_authentication()

		# Perform the usual authentication
		return super().get_current_user()

	def check_xsrf_cookie(self):
		# Skip the check on PUT
		if self.request.method == "PUT":
			return

		# Perform the check as usual
		super().check_xsrf_cookie()

	def write_error(self, *args, **kwargs):
		if self.request.method == "PUT":
			return

		# Write errors as usual
		return super().write_error(*args, **kwargs)

	@tornado.web.authenticated
	@base.ratelimit(minutes=15, requests=5)
	def put(self):
		with self.db.transaction():
			paste = self.backend.nopaste.create(
				self.request.body, account=self.current_user, address=self.get_remote_ip())

		# Send a message to the client
		self.write("https://%s/view/%s\n" % (self.request.host, paste.uuid))

		# Tell the user when this expires
		if paste.expires_at:
			self.write("  This paste will expire at %s\n" % self.locale.format_date(paste.expires_at))

		# All done
		self.finish()


class UploadHandler(base.AnalyticsMixin, base.BaseHandler):
	@tornado.web.authenticated
	def get(self):
		self.render("nopaste/upload.html")

	@tornado.web.authenticated
	def post(self):
		subject = self.get_argument("subject", None)

		# Fetch expires time
		expires = self.get_argument_int("expires", "0")

		with self.db.transaction():
			for f in self.request.files.get("file"):
				paste = self.backend.nopaste.create(f.body, subject=subject, expires=expires,
					account=self.current_user, address=self.get_remote_ip())

				# Only accept one file
				break

		# Redirect to the paste
		return self.redirect("/view/%s" % paste.uuid)


class RawHandler(base.AnalyticsMixin, base.BaseHandler):
	def get(self, uid):
		with self.db.transaction():
			paste = self.backend.nopaste.get(uid)
			if not paste:
				raise tornado.web.HTTPError(404)

			# This has received a view
			paste.viewed()

		# Set the filename
		self.set_header("Content-Disposition", "inline; filename=\"%s\"" % paste.subject)

		# Set mimetype
		self.set_header("Content-Type", paste.mimetype)

		# Send content
		self.finish(paste.blob)


class ViewHandler(base.AnalyticsMixin, base.BaseHandler):
	def get(self, uid):
		with self.db.transaction():
			paste = self.backend.nopaste.get(uid)
			if not paste:
				raise tornado.web.HTTPError(404)

			# This has received a view
			paste.viewed()

		self.render("nopaste/view.html", paste=paste)


class CodeModule(ui_modules.UIModule):
	def render(self, content):
		return self.render_string("nopaste/modules/code.html", content=content)

	def javascript_files(self):
		return "js/prettify.js"

	def css_files(self):
		return "css/prettify.css"

	def embedded_javascript(self):
		return """
			// Run pretty print
			prettyPrint();
		"""
