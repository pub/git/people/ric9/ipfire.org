#!/usr/bin/python

import datetime
import dateutil
import email.utils
import tornado.web

from . import base
from . import ui_modules

class IndexHandler(base.AnalyticsMixin, base.BaseHandler):
	def get(self):
		latest_post = None

		# Fetch the search query
		q = self.get_argument("q", None)

		# If the user is searching, perform the search
		if q:
			posts = self.backend.blog.search(q)

		# Otherwise fetch the latest posts
		else:
			posts = self.backend.blog.get_newest(limit=10)

			# Extract the latest post
			latest_post = posts.pop(0)

		self.render("blog/index.html", q=q, posts=posts, latest_post=latest_post)


class FeedHandler(base.AnalyticsMixin, base.BaseHandler):
	def get(self):
		posts = self.backend.blog.get_newest(limit=10)
		if not posts:
			raise tornado.web.HTTPError(404)

		# Allow this to be cached for 10 minutes
		self.set_expires(600)

		# Set correct content type
		self.set_header("Content-Type", "application/atom+xml")

		# Render the feed
		self.render("blog/feed.xml", posts=posts,
			now=datetime.datetime.now())


class PostHandler(base.AnalyticsMixin, base.BaseHandler):
	def get(self, slug):
		post = self.backend.blog.get_by_slug(slug)
		if not post:
			raise tornado.web.HTTPError(404)

		self.render("blog/post.html", post=post)


class PublishHandler(base.BaseHandler):
	@tornado.web.authenticated
	def prepare(self):
		# Check if the user has permissions
		if not self.current_user.is_blog_author():
			raise tornado.web.HTTPError(403)

	@tornado.web.authenticated
	def get(self, slug):
		post = self.backend.blog.get_by_slug(slug)
		if not post:
			raise tornado.web.HTTPError(404)

		# Check if current_user is allowed to edit the post
		if not post.is_editable(self.current_user):
			raise tornado.web.HTTPError(403)

		# Is the post already published?
		if post.is_published():
			raise tornado.web.HTTPError(400, "Post is already published")

		self.render("blog/publish.html", post=post)

	@tornado.web.authenticated
	def post(self, slug):
		post = self.backend.blog.get_by_slug(slug)
		if not post:
			raise tornado.web.HTTPError(404)

		# Check if current_user is allowed to edit the post
		if not post.is_editable(self.current_user):
			raise tornado.web.HTTPError(403)

		# Is the post already published?
		if post.is_published():
			raise tornado.web.HTTPError(400, "Post is already published")

		when = self.get_argument("when", None)
		if when:
			when = dateutil.parser.parse(when)

		# Publish the post
		with self.db.transaction():
			post.publish(when)

		self.redirect("/blog/%s" % post.slug)


class DraftsHandler(base.BaseHandler):
	@tornado.web.authenticated
	def prepare(self):
		# Check if the user has permissions
		if not self.current_user.is_blog_author():
			raise tornado.web.HTTPError(403)

	@tornado.web.authenticated
	def get(self):
		drafts = self.backend.blog.get_drafts(author=self.current_user)

		self.render("blog/drafts.html", drafts=drafts)


class YearHandler(base.AnalyticsMixin, base.BaseHandler):
	def get(self, year):
		posts = self.backend.blog.get_by_year(year)
		if not posts:
			raise tornado.web.HTTPError(404, "There are no posts in %s" % year)

		self.render("blog/year.html", posts=posts, year=year)


class WriteHandler(base.BaseHandler):
	@tornado.web.authenticated
	def prepare(self):
		# Check if the user has permissions
		if not self.current_user.is_blog_author():
			raise tornado.web.HTTPError(403)

	@tornado.web.authenticated
	def get(self):
		self.render("blog/write.html", post=None)

	@tornado.web.authenticated
	def post(self):
		title = self.get_argument("title")
		text  = self.get_argument("text")
		tags  = self.get_argument("tags", "").split(" ")

		with self.db.transaction():
			post = self.backend.blog.create_post(title, text,
				author=self.current_user, tags=tags)

		# Redirect to the new post
		self.redirect("/blog/%s" % post.slug)


class EditHandler(base.BaseHandler):
	@tornado.web.authenticated
	def get(self, slug):
		post = self.backend.blog.get_by_slug(slug)
		if not post:
			raise tornado.web.HTTPError(404)

		# Check if post is editable
		if not post.is_editable(self.current_user):
			raise tornado.web.HTTPError(403, "%s cannot edit %s" % (self.current_user, post))

		self.render("blog/write.html", post=post)

	@tornado.web.authenticated
	def post(self, slug):
		post = self.backend.blog.get_by_slug(slug)
		if not post:
			raise tornado.web.HTTPError(404, "Could not find post %s" % slug)

		# Check if post is editable
		if not post.is_editable(self.current_user):
			raise tornado.web.HTTPError(403, "%s cannot edit %s" % (self.current_user, post))

		# Save updated content
		with self.db.transaction():
			post.update(
				title = self.get_argument("title"),
				text  = self.get_argument("text"),
				tags  = self.get_argument("tags", "").split(" "),
			)

		# Redirect to the post
		self.redirect("/blog/%s" % post.slug)


class DeleteHandler(base.BaseHandler):
	@tornado.web.authenticated
	def get(self, slug):
		post = self.backend.blog.get_by_slug(slug)
		if not post:
			raise tornado.web.HTTPError(404)

		# Check if post is editable
		if not post.is_editable(self.current_user):
			raise tornado.web.HTTPError(403, "%s cannot edit %s" % (self.current_user, post))

		self.render("blog/delete.html", post=post)

	@tornado.web.authenticated
	def post(self, slug):
		post = self.backend.blog.get_by_slug(slug)
		if not post:
			raise tornado.web.HTTPError(404)

		# Check if post is editable
		if not post.is_editable(self.current_user):
			raise tornado.web.HTTPError(403, "%s cannot edit %s" % (self.current_user, post))

		with self.db.transaction():
			post.delete()

		# Return to drafts
		self.redirect("/drafts")


class DebugEmailHandler(base.BaseHandler):
	@tornado.web.authenticated
	def get(self, slug):
		if not self.current_user.is_staff():
			raise tornado.web.HTTPError(403)

		# Fetch the post
		post = self.backend.blog.get_by_slug(slug)
		if not post:
			raise tornado.web.HTTPError(404, "Could not find post %s" % slug)

		self.render("blog/messages/announcement.html", account=self.current_user, post=post)


class HistoryNavigationModule(ui_modules.UIModule):
	def render(self):
		return self.render_string("blog/modules/history-navigation.html",
			years=self.backend.blog.years)


class ListModule(ui_modules.UIModule):
	def render(self, posts, relative=False, show_author=True):
		return self.render_string("blog/modules/list.html",
			posts=posts, relative=relative, show_author=show_author)
