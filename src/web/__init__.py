#/usr/bin/python

import logging
import os.path
import phonenumbers
import phonenumbers.geocoder
import tornado.locale
import tornado.options
import tornado.web

import ipfire
import ipfire.countries
from .. import util

from .handlers import *

from . import analytics
from . import auth
from . import blog
from . import boot
from . import docs
from . import donate
from . import downloads
from . import fireinfo
from . import iuse
from . import lists
from . import location
from . import nopaste
from . import ui_modules
from . import users
from . import voip

class Application(tornado.web.Application):
	def __init__(self, config, **kwargs):
		# Initialize backend
		self.backend = ipfire.Backend(config)

		settings = {
			# Do not compress responses
			"gzip" : False,

			# Enable XSRF cookies
			"xsrf_cookies" : True,
			"xsrf_cookie_kwargs" : {
				"secure" : True,
			},

			# Login
			"login_url" : "/login",

			# Setup directory structure
			"static_path"   : self.backend.config.get("global", "static_dir"),
			"template_path" : self.backend.config.get("global", "templates_dir"),

			# UI Methods
			"ui_methods" : {
				"format_country_name"          : self.format_country_name,
				"format_language_name"         : self.format_language_name,
				"format_month_name"            : self.format_month_name,
				"format_phone_number"          : self.format_phone_number,
				"format_phone_number_to_e164"  : self.format_phone_number_to_e164,
				"format_phone_number_location" : self.format_phone_number_location,
			},

			# UI Modules
			"ui_modules" : {
				# Analytics
				"AnalyticsSummary"     : analytics.SummaryModule,

				# Auth
				"Password"             : auth.PasswordModule,

				# Blog
				"BlogHistoryNavigation": blog.HistoryNavigationModule,
				"BlogList"             : blog.ListModule,

				# Boot
				"BootMenuConfig"       : boot.MenuConfigModule,
				"BootMenuHeader"       : boot.MenuHeaderModule,
				"BootMenuSeparator"    : boot.MenuSeparatorModule,

				# Docs
				"DocsDiff"             : docs.DiffModule,
				"DocsHeader"           : docs.HeaderModule,
				"DocsList"             : docs.ListModule,

				# Nopaste
				"Code"                 : nopaste.CodeModule,

				# Fireinfo
				"FireinfoDeviceTable"  : fireinfo.DeviceTableModule,
				"FireinfoDeviceAndGroupsTable"
				                       : fireinfo.DeviceAndGroupsTableModule,

				# Users
				"UsersList"            : users.ListModule,

				# VoIP
				"VoIPConferences"      : voip.ConferencesModule,
				"VoIPOutboundRegistrations" :
					voip.OutboundRegistrationsModule,
				"VoIPQueues"           : voip.QueuesModule,
				"VoIPRegistrations"    : voip.RegistrationsModule,

				# Misc
				"IPFireLogo"           : ui_modules.IPFireLogoModule,
				"Markdown"             : ui_modules.MarkdownModule,
				"Map"                  : ui_modules.MapModule,
				"ProgressBar"          : ui_modules.ProgressBarModule,
			},

			# Call this when a page wasn't found
			"default_handler_class" : base.NotFoundHandler,
		}
		settings.update(kwargs)

		tornado.web.Application.__init__(self, **settings)

		authentication_handlers = [
			(r"/login", auth.LoginHandler),
			(r"/logout", auth.LogoutHandler),
		]

		self.add_handlers(r"www\.([a-z]+\.dev\.)?ipfire\.org", [
			# Entry site that lead the user to index
			(r"/", IndexHandler),

			# Analytics
			(r"/analytics", analytics.IndexHandler),
			(r"/analytics/docs", analytics.DocsHandler),

			# Authentication
			(r"/join", auth.JoinHandler),
			(r"/login", auth.LoginHandler),
			(r"/logout", auth.LogoutHandler),
			(r"/activate/([a-z_][a-z0-9_-]{0,31})/(\w+)", auth.ActivateHandler),

			# Blog
			(r"/blog", blog.IndexHandler),
			(r"/blog/drafts", blog.DraftsHandler),
			(r"/blog/feed.xml", blog.FeedHandler),
			(r"/blog/write", blog.WriteHandler),
			(r"/blog/years/([0-9]{4})", blog.YearHandler),
			(r"/blog/([0-9a-z\-\._]+)", blog.PostHandler),
			(r"/blog/([0-9a-z\-\._]+)/delete", blog.DeleteHandler),
			(r"/blog/([0-9a-z\-\._]+)/edit", blog.EditHandler),
			(r"/blog/([0-9a-z\-\._]+)/publish", blog.PublishHandler),
			(r"/blog/([0-9a-z\-\._]+)/debug/email", blog.DebugEmailHandler),

			# Docs
			(r"/docs/recent\-changes", docs.RecentChangesHandler),
			(r"/docs/search", docs.SearchHandler),
			(r"/docs/tree", docs.TreeHandler),
			(r"/docs/watchlist", docs.WatchlistHandler),
			(r"/docs/_restore", docs.RestoreHandler),
			(r"/docs/_upload", docs.UploadHandler),
			(r"/docs(/[A-Za-z0-9\-_\/]+)?/_edit", docs.EditHandler),
			(r"/docs(/[A-Za-z0-9\-_\/]+)?/_render", docs.RenderHandler),
			(r"/docs(/[A-Za-z0-9\-_\/]+)?/_(watch|unwatch)", docs.WatchHandler),
			(r"/docs(/[A-Za-z0-9\-_\/]+)?/_files", docs.FilesHandler),
			(r"/docs(/[A-Za-z0-9\-_\/]+(?:.*)\.(?:\w+))/_delete", docs.DeleteFileHandler),
			(r"/docs(/[A-Za-z0-9\-_\/]+(?:.*)\.(?:\w+))$", docs.FileHandler),
			(r"/docs(/[A-Za-z0-9\-_\/]*)?", docs.PageHandler),

			# Downloads
			(r"/downloads", downloads.IndexHandler),
			(r"/downloads/cloud", StaticHandler, { "template" : "downloads/cloud.html" }),
			(r"/downloads/mirrors", downloads.MirrorsHandler),
			(r"/downloads/thank-you", downloads.ThankYouHandler),
			(r"/downloads/([0-9a-z\-\.]+)", downloads.ReleaseHandler),

			# Donate
			(r"/donate", donate.DonateHandler),
			(r"/donate/thank-you", donate.ThankYouHandler),
			(r"/donate/error", donate.ErrorHandler),
			(r"/donate/check-vat-number", donate.CheckVATNumberHandler),

			# Fireinfo
			(r"/fireinfo", fireinfo.IndexHandler),
			(r"/fireinfo/admin", fireinfo.AdminIndexHandler),
			(r"/fireinfo/vendors", fireinfo.VendorsHandler),
			(r"/fireinfo/vendors/(pci|usb)/([0-9a-f]{4})", fireinfo.VendorHandler),
			(r"/fireinfo/drivers/(.*)", fireinfo.DriverDetail),
			(r"/fireinfo/profile/random", fireinfo.RandomProfileHandler),
			(r"/fireinfo/profile/([a-z0-9]{40})", fireinfo.ProfileHandler),
			(r"/fireinfo/processors", fireinfo.ProcessorsHandler),
			(r"/fireinfo/releases", fireinfo.ReleasesHandler),
			(r"/fireinfo/send/([a-z0-9]+)", fireinfo.ProfileSendHandler),
			(r"/fireinfo/static/(.*)", tornado.web.StaticFileHandler, { "path" : self.settings.get("static_path") }),

			# Lists
			(r"/lists", lists.IndexHandler),

			# Password Reset
			(r"/password\-reset", auth.PasswordResetInitiationHandler),
			(r"/password\-reset/([a-z_][a-z0-9_-]{0,31})/(\w+)", auth.PasswordResetHandler),
			(r"/.well-known/change-password", auth.WellKnownChangePasswordHandler),

			# Projects
			(r"/location/?", location.IndexHandler),
			(r"/location/download", StaticHandler, { "template" : "location/download.html" }),
			(r"/location/how\-to\-use", StaticHandler, { "template" : "location/how-to-use.html" }),
			(r"/location/lookup/(.+)", location.LookupHandler),

			# Single-Sign-On for Discourse
			(r"/sso/discourse", auth.SSODiscourse),

			# User Groups
			(r"/users/groups", users.GroupIndexHandler),
			(r"/users/groups/([a-z_][a-z0-9_-]{0,31})", users.GroupShowHandler),

			# Users
			(r"/users", users.IndexHandler),
			(r"/users/([a-z_][a-z0-9_-]{0,31})", users.ShowHandler),
			(r"/users/([a-z_][a-z0-9_-]{0,31})\.jpg", users.AvatarHandler),
			(r"/users/([a-z_][a-z0-9_-]{0,31})/delete", users.DeleteHandler),
			(r"/users/([a-z_][a-z0-9_-]{0,31})/edit", users.EditHandler),
			(r"/users/([a-z_][a-z0-9_-]{0,31})/passwd", users.PasswdHandler),

			# Promotional Consent Stuff
			(r"/subscribe", users.SubscribeHandler),
			(r"/unsubscribe", users.UnsubscribeHandler),

			# VoIP
			(r"/voip", voip.IndexHandler),

			# Static Pages
			(r"/about",  StaticHandler, { "template" : "static/about.html" }),
			(r"/legal", StaticHandler, { "template" : "static/legal.html" }),
			(r"/help", StaticHandler, { "template" : "static/help.html" }),
			(r"/partners", StaticHandler, { "template" : "static/partners.html" }),
			(r"/sitemap",  StaticHandler, { "template" : "static/sitemap.html" }),

			# API
			(r"/api/check/email", auth.APICheckEmail),
			(r"/api/check/uid", auth.APICheckUID),

			# Handle old pages that have moved elsewhere
			(r"/blog/authors/(\w+)", tornado.web.RedirectHandler, { "url" : "/users/{0}" }),
			(r"/donation", tornado.web.RedirectHandler, { "url" : "/donate" }),
			(r"/download", tornado.web.RedirectHandler, { "url" : "/downloads" }),
			(r"/download/([0-9a-z\-\.]+)", tornado.web.RedirectHandler, { "url" : "/downloads/{0}" }),
			(r"/features", tornado.web.RedirectHandler, { "url" : "/about" }),
			(r"/imprint", tornado.web.RedirectHandler, { "url" : "/legal" }),
			(r"/news.rss", tornado.web.RedirectHandler, { "url" : "/blog/feed.xml" }),
			(r"/news/(.*)", tornado.web.RedirectHandler, { "url" : "/blog/{0}" }),
			(r"/support", tornado.web.RedirectHandler, { "url" : "/help"}),
			(r"/(de|en)/(.*)", tornado.web.RedirectHandler, { "url" : "/{0}"}),

			# Export arbitrary error pages
			(r"/error/([45][0-9]{2})", base.ErrorHandler),

			# Serve any static files
			(r"/static/(.*)", tornado.web.StaticFileHandler, { "path" : self.settings.get("static_path") }),
		])

		# blog.ipfire.org - LEGACY REDIRECTION
		self.add_handlers(r"blog\.([a-z]+\.dev\.)?ipfire\.org", [
			(r"/", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/blog" }),
			(r"/authors/(\w+)", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/blog/authors/{0}" }),
			(r"/post/([0-9a-z\-\._]+)", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/blog/{0}" }),
			(r"/tags/([0-9a-z\-\.]+)", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/blog/tags/{0}" }),

			# RSS Feed
			(r"/feed.xml", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/blog/feed.xml" }),
		])

		# downloads.ipfire.org
		self.add_handlers(r"downloads\.([a-z]+\.dev\.)?ipfire\.org", [
			(r"/", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/download" }),
			(r"/release/(.*)",  tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/download/{0}" }),
			(r"/(.*)", downloads.FileHandler),
		])

		# mirrors.ipfire.org
		self.add_handlers(r"mirrors\.([a-z]+\.dev\.)?ipfire\.org", [
			(r"/", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/download/mirrors" }),
			(r"/mirrors/(.*)", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/download/mirrors/{0}" }),
		])

		# planet.ipfire.org
		self.add_handlers(r"planet\.([a-z]+\.dev\.)?ipfire\.org", [
			(r"/", tornado.web.RedirectHandler, { "url" : "https://blog.ipfire.org/" }),
			(r"/post/([A-Za-z0-9_-]+)", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/blog/{0}" }),
			(r"/user/([a-z0-9_-]+)", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/blog/authors/{0}" }),

			# RSS
			(r"/rss", tornado.web.RedirectHandler, { "url" : "https://blog.ipfire.org/feed.xml" }),
			(r"/user/([a-z0-9_-]+)/rss", tornado.web.RedirectHandler, { "url" : "https://blog.ipfire.org/feed.xml" }),
			(r"/news.rss", tornado.web.RedirectHandler, { "url" : "https://blog.ipfire.org/feed.xml" }),
		])

		# fireinfo.ipfire.org - LEGACY REDIRECTION
		self.add_handlers(r"fireinfo\.([a-z]+\.dev\.)?ipfire\.org", [
			(r"/", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/fireinfo/" }),

			# Admin
			(r"/admin", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/fireinfo/admin" }),

			# Vendors
			(r"/vendors", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/fireinfo/vendors" }),
			(r"/vendors/(pci|usb)/([0-9a-f]{4})", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/fireinfo/vendors/(pci|usb)/([0-9a-f]{4})" }),

			# Driver
			(r"/drivers/(.*)", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/fireinfo/drivers/(.*)" }),

			# Show profiles
			(r"/profile/random", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/fireinfo/profile/random" }),
			(r"/profile/([a-z0-9]{40})", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/fireinfo/profile/([a-z0-9]{40})" }),

			# Stats
			(r"/processors", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/fireinfo/processors" }),
			(r"/releases", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/fireinfo/releases" }),

			# Send profiles
			(r"/send/([a-z0-9]+)", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/fireinfo/send/([a-z0-9]+)" }),

			# Serve any static files
			(r"/static/(.*)", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/fireinfo/static/(.*)" }),
		] + authentication_handlers)

		# i-use.ipfire.org
		self.add_handlers(r"i-use\.([a-z]+\.dev\.)?ipfire\.org", [
			(r"/", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/" }),
			(r"/profile/([a-f0-9]{40})/([0-9]+).png", iuse.ImageHandler),
		])

		# boot.ipfire.org
		BOOT_STATIC_PATH = os.path.join(self.settings["static_path"], "netboot")
		self.add_handlers(r"boot\.([a-z]+\.dev\.)?ipfire\.org", [
			(r"/", tornado.web.RedirectHandler, { "url" : "https://wiki.ipfire.org/installation/pxe" }),

			# Configurations
			(r"/premenu.cfg", boot.PremenuCfgHandler),
			(r"/menu.gpxe", boot.MenuGPXEHandler),
			(r"/menu.cfg", boot.MenuCfgHandler),

			# Static files
			(r"/(boot\.png|pxelinux\.0|menu\.c32|vesamenu\.c32)",
				tornado.web.StaticFileHandler, { "path" : BOOT_STATIC_PATH }),
		])

		# nopaste.ipfire.org
		self.add_handlers(r"nopaste\.([a-z]+\.dev\.)?ipfire\.org", [
			(r"/", nopaste.CreateHandler),
			(r"/upload", nopaste.UploadHandler),

			# View
			(r"/raw/(.*)", nopaste.RawHandler),
			(r"/view/(.*)", nopaste.ViewHandler),

			# Serve any static files
			(r"/static/(.*)", tornado.web.StaticFileHandler, { "path" : self.settings.get("static_path") }),
		] + authentication_handlers)

		# location.ipfire.org and /projects/location
		self.add_handlers(r"location\.([a-z]+\.dev\.)?ipfire\.org", [
			(r"(.*)", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/location{0}" }),
		])
		self.add_handlers(r"www\.([a-z]+\.dev\.)?ipfire\.org", [
			(r"/projects/location/(.*)", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/location{0}" }),
		])

		# geoip.ipfire.org
		self.add_handlers(r"geoip\.([a-z]+\.dev\.)?ipfire\.org", [
			(r"/", tornado.web.RedirectHandler, { "url" : "https://location.ipfire.org/" }),
		])

		# talk.ipfire.org
		self.add_handlers(r"talk\.([a-z]+\.dev\.)?ipfire\.org", [
			(r"/", tornado.web.RedirectHandler, { "url" : "https://people.ipfire.org/" }),
		])

		# people.ipfire.org
		self.add_handlers(r"people\.([a-z]+\.dev\.)?ipfire\.org", [
			(r"/", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/users" }),
			(r"/register", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/join" }),
			(r"/users", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/users" }),
			(r"/users/([a-z_][a-z0-9_-]{0,31})", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/users/{0}" }),
			(r"/users/([a-z_][a-z0-9_-]{0,31})\.jpg", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/users/{0}.jpg" }),
		])

		# wiki.ipfire.org
		self.add_handlers(r"wiki\.([a-z]+\.dev\.)?ipfire\.org", [
			(r"(.*)", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/docs{0}" }),
		])

		# ipfire.org
		self.add_handlers(r"([a-z]+\.dev\.)?ipfire\.org", [
			(r".*", tornado.web.RedirectHandler, { "url" : "https://www.ipfire.org/" })
		])

		logging.info("Successfully initialied application")

	def format_country_name(self, handler, country_code):
		return self.backend.get_country_name(country_code)

	def format_language_name(self, handler, language):
		_ = handler.locale.translate

		if language == "de":
			return _("German")
		elif language == "en":
			return _("English")
		elif language == "es":
			return _("Spanish")
		elif language == "fr":
			return _("French")
		elif language == "it":
			return _("Italian")
		elif language == "nl":
			return _("Dutch")
		elif language == "pl":
			return _("Polish")
		elif language == "pt":
			return _("Portuguese")
		elif language == "ru":
			return _("Russian")
		elif language == "tr":
			return _("Turkish")

		return language

	def format_month_name(self, handler, month):
		_ = handler.locale.translate

		if month == 1:
			return _("January")
		elif month == 2:
			return _("February")
		elif month == 3:
			return _("March")
		elif month == 4:
			return _("April")
		elif month == 5:
			return _("May")
		elif month == 6:
			return _("June")
		elif month == 7:
			return _("July")
		elif month == 8:
			return _("August")
		elif month == 9:
			return _("September")
		elif month == 10:
			return _("October")
		elif month == 11:
			return _("November")
		elif month == 12:
			return _("December")

		return month

	def format_phone_number(self, handler, number):
		if not isinstance(number, phonenumbers.PhoneNumber):
			try:
				number = phonenumbers.parse(number, None)
			except phonenumbers.phonenumberutil.NumberParseException:
				return number

		return phonenumbers.format_number(number, phonenumbers.PhoneNumberFormat.INTERNATIONAL)

	def format_phone_number_to_e164(self, handler, number):
		return phonenumbers.format_number(number, phonenumbers.PhoneNumberFormat.E164)

	def format_phone_number_location(self, handler, number):
		s = [
			phonenumbers.geocoder.description_for_number(number, handler.locale.code),
			phonenumbers.region_code_for_number(number),
		]

		return ", ".join((e for e in s if e))
