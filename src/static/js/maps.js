$(function() {
	$(".map").each(function() {
		var map = L.map(this);

		// Add the map provider
		var layer = L.tileLayer("https://a.tile.openstreetmap.org/{z}/{x}/{y}.png", {
			attribution: "&copy; <a href=\"https://osm.org/copyright\">OpenStreetMap</a> contributors"
		}).addTo(map);

		var search = $(this).attr("data-map-search");
		if (search) {
			var provider = new L.Control.Geocoder.Nominatim();

			console.log("search" + search);

			provider.geocode(search, function (results) {
				$.each(results, function(i, result) {
					// Move map center
					map.fitBounds(result.bbox);

					var marker = L.marker(result.center);
					marker.addTo(map);

					// End loop after first result
					return false;
				});
			});
		}
	});
});
