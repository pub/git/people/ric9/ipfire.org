class Editor {
	constructor(parent) {
		this.parent = $(parent);
		this.data = this.parent.data();

		// Hide the preview here
		this.preview = $("#preview");
		this.preview.hide();

		// Get the textarea
		this.textarea = this.parent.find("textarea");

		// Initialise selection
		this.selection = {
			start  : 0,
			end    : 0,
			text   : "",
			length : 0,
		};

		// Make the textarea magic
		this.activateTextArea();

		// Bind all keys
		this.bindKeys();

		// Change timer for preview
		var update = null;

		console.log("Editor initialised for " + this.parent);

		// Set focus on the textarea
		this.textarea.focus();
	}

	activateTextArea() {
		var editor = this;

		// Render preview when content has changed
		this.textarea.on("keyup change", function(e) {
			if (editor.update)
				clearTimeout(editor.update);

			var content = editor.textarea.val();

			// If the field is all empty, we will hide it
			if (content)
				editor.preview.show();
			else
				editor.preview.hide();

			// Go into update mode
			editor.preview.addClass("updating");

			// Render content and show it
			editor.update = setTimeout(function() {
				var c = $("#preview-content");

				$.post(editor.data.render_url, { content : content },
					function(data) {
						c.html(data);

						// Update finished
						editor.preview.removeClass("updating");
					}
				);
			}, 750);
		});

		// Remember any selected text
		this.textarea.on("select keyup click", function(e) {
			// Ignore any keyboard shortcuts
			if (e.ctrlKey)
				return;

			// Save selected text
			editor.selection = {
				start : this.selectionStart,
				end   : this.selectionEnd,
				text  : this.value.slice(this.selectionStart, this.selectionEnd),
				length: this.selectionEnd - this.selectionStart,
			};

			console.debug("Something got selected:");
			console.debug(editor.selection);
		})

		// Bind keyboard shortcuts
		this.textarea.on("keyup", function(e) {
			// If Ctrl wasn't pressed this isn't for us
			if (!e.ctrlKey)
				return;

			switch (e.which) {
				// B - Bold
				case 66:
					editor.bold();
					break;

				// I - Italic
				case 73:
					editor.italic();
					break;

				// C - Code
				case 67:
					editor.code();
					break;

				// H - Headline
				case 72:
					editor.headline();
					break;

				// L - Link
				case 76:
					editor.link();
					break;
			}
		});
	}

	bindKeys() {
		// Typography
		this.parent.find("#bold").click(this.bold.bind(this));
		this.parent.find("#italic").click(this.italic.bind(this));
		this.parent.find("#code").click(this.code.bind(this));

		// Headlines
		this.parent.find("#headline").click(this.headline.bind(this));
		this.parent.find("#headline-down").click(this.headline_down.bind(this));
		this.parent.find("#headline-up").click(this.headline_up.bind(this));

		// Links
		this.parent.find("#link").click(this.link.bind(this));
	}

	// Functions to modify the text

	replaceSelection(replacement) {
		// Get the DOM element
		var textarea = this.textarea.get(0);

		// Write text to textarea and move the cursor to the end
		textarea.setRangeText(replacement,
			this.selection.start, this.selection.end, "end");
	}

	insertAtCursor(insertion) {
		this.replaceSelection(insertion);
	}

	bold() {
		console.debug("Converting into bold: " + this.selection.text);
		this.replaceSelection("**" + this.selection.text + "**");
	}

	italic() {
		console.debug("Converting into italic: " + this.selection.text);
		this.replaceSelection("*" + this.selection.text + "*");
	}

	code() {
		var multiline = this.selection.text.indexOf("\n");

		if (multiline >= 0) {
			this.replaceSelection("```\n" + this.selection.text + "\n```\n\n");
		} else {
			this.replaceSelection("`" + this.selection.text + "`");
		}
	}

	link() {
		// Handle URLs
		if (this.selection.text.startsWith("https://") || this.selection.text.startsWith("http://")) {
			this.replaceSelection("[" + this.selection.text + "](" + this.selection.text + ")");
		// Handle selected text
		} else {
			this.replaceSelection("[" + this.selection.text + "]()")
		}
	}

	// Headlines

	findLevel() {
		// Get all text between start and cursor position
		var text = this.textarea.val().slice(0, this.selection.start);

		// Split it in lines and reverse
		var lines = text.split("\n");
		lines.reverse();

		for (var line of lines) {
			console.debug(line);

			// Return the number of # found in the nearest headline
			var match = line.match("^(#+)");
			if (match) {
				return match[1].length;
			}
		}

		// If nothing was found, we are on level one
		return 1;
	}

	insertHeadline(offset) {
		// Find level of headlines
		var level = Math.max(this.findLevel() + offset, 1);

		console.debug("Adding headline (" + level + ")");
		var headline = "#".repeat(level);

		if (this.selection.length == 0) {
			headline += " ...";
		} else {
			// Add some space if we don't have any, yet
			if (!this.selection.text.startsWith(" "))
				headline += " ";

			headline += this.selection.text + "\n\n";
		}

		this.replaceSelection(headline);
	}

	headline() {
		return this.insertHeadline(0);
	}

	headline_down() {
		return this.insertHeadline(1);
	}

	headline_up() {
		return this.insertHeadline(-1);
	}
}

$(document).ready(function() {
	// Initialise all editors
	$(".editor").each(function(i, e) {
		new Editor(e);
	});
});
