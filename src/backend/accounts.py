#!/usr/bin/python
# encoding: utf-8

import asyncio
import base64
import datetime
import hashlib
import hmac
import iso3166
import json
import kerberos
import ldap
import ldap.modlist
import logging
import os
import phonenumbers
import re
import socket
import sys
import time
import tornado.httpclient
import urllib.parse
import urllib.request
import zxcvbn

from . import countries
from . import util
from .decorators import *
from .misc import Object

# Set the client keytab name
os.environ["KRB5_CLIENT_KTNAME"] = "/etc/ipfire.org/ldap.keytab"

FQDN = socket.gethostname()

class LDAPObject(Object):
	def init(self, dn, attrs=None):
		self.dn = dn

		self.attributes = attrs or {}

	def __eq__(self, other):
		if isinstance(other, self.__class__):
			return self.dn == other.dn

		return NotImplemented

	@property
	def ldap(self):
		return self.accounts.ldap

	def _exists(self, key):
		try:
			self.attributes[key]
		except KeyError:
			return False

		return True

	def _get(self, key):
		for value in self.attributes.get(key, []):
			yield value

	def _get_bytes(self, key, default=None):
		for value in self._get(key):
			return value

		return default

	def _get_strings(self, key):
		for value in self._get(key):
			yield value.decode()

	def _get_string(self, key, default=None):
		for value in self._get_strings(key):
			return value

		return default

	def _get_phone_numbers(self, key):
		for value in self._get_strings(key):
			yield phonenumbers.parse(value, None)

	def _get_timestamp(self, key):
		value = self._get_string(key)

		# Parse the timestamp value and returns a datetime object
		if value:
			return datetime.datetime.strptime(value, "%Y%m%d%H%M%SZ")

	def _modify(self, modlist):
		logging.debug("Modifying %s: %s" % (self.dn, modlist))

		# Authenticate before performing any write operations
		self.accounts._authenticate()

		# Run modify operation
		self.ldap.modify_s(self.dn, modlist)

	def _set(self, key, values):
		current = self._get(key)

		# Don't do anything if nothing has changed
		if list(current) == values:
			return

		# Remove all old values and add all new ones
		modlist = []

		if self._exists(key):
			modlist.append((ldap.MOD_DELETE, key, None))

		# Add new values
		if values:
			modlist.append((ldap.MOD_ADD, key, values))

		# Run modify operation
		self._modify(modlist)

		# Update cache
		self.attributes.update({ key : values })

	def _set_bytes(self, key, values):
		return self._set(key, values)

	def _set_strings(self, key, values):
		return self._set(key, [e.encode() for e in values if e])

	def _set_string(self, key, value):
		return self._set_strings(key, [value,])

	def _add(self, key, values):
		modlist = [
			(ldap.MOD_ADD, key, values),
		]

		self._modify(modlist)

	def _add_strings(self, key, values):
		return self._add(key, [e.encode() for e in values])

	def _add_string(self, key, value):
		return self._add_strings(key, [value,])

	def _delete(self, key, values):
		modlist = [
			(ldap.MOD_DELETE, key, values),
		]

		self._modify(modlist)

	def _delete_strings(self, key, values):
		return self._delete(key, [e.encode() for e in values])

	def _delete_string(self, key, value):
		return self._delete_strings(key, [value,])

	def _delete_dn(self, dn):
		logging.debug("Deleting %s" % dn)

		# Authenticate before performing any delete operations
		self.accounts._authenticate()

		# Run delete operation
		self.ldap.delete_s(dn)

	@property
	def objectclasses(self):
		return self._get_strings("objectClass")

	@staticmethod
	def _parse_date(s):
		return datetime.datetime.strptime(s.decode(), "%Y%m%d%H%M%SZ")


class Accounts(Object):
	def init(self):
		self.search_base = self.settings.get("ldap_search_base")

	def __len__(self):
		return self._count("(objectClass=person)")

	def __iter__(self):
		accounts = self._search("(objectClass=person)")

		return iter(sorted(accounts))

	@lazy_property
	def ldap(self):
		# Connect to LDAP server
		ldap_uri = self.settings.get("ldap_uri")

		logging.debug("Connecting to LDAP server: %s" % ldap_uri)

		# Connect to the LDAP server
		connection = ldap.ldapobject.ReconnectLDAPObject(ldap_uri,
			trace_level=2 if self.backend.debug else 0,
			retry_max=sys.maxsize, retry_delay=3)

		# Set maximum timeout for operations
		connection.set_option(ldap.OPT_TIMEOUT, 10)

		return connection

	def _authenticate(self):
		# Authenticate against LDAP server using Kerberos
		self.ldap.sasl_gssapi_bind_s()

	async def test_ldap(self):
		logging.info("Testing LDAP connection...")

		self._authenticate()

		logging.info("Successfully authenticated as %s" % self.ldap.whoami_s())

	def _query(self, query, attrlist=None, limit=0, search_base=None):
		logging.debug("Performing LDAP query (%s): %s" \
			% (search_base or self.search_base, query))

		t = time.time()

		# Ask for up to 512 results being returned at a time
		page_control = ldap.controls.SimplePagedResultsControl(True, size=512, cookie="")

		results = []
		pages = 0

		# Perform the search
		while True:
			response = self.ldap.search_ext(search_base or self.search_base,
				ldap.SCOPE_SUBTREE, query, attrlist=attrlist, sizelimit=limit,
				serverctrls=[page_control],
			)

			# Fetch all results
			type, data, rmsgid, serverctrls = self.ldap.result3(response)

			# Append to local copy
			results += data
			pages += 1

			controls = [c for c in serverctrls
				if c.controlType == ldap.controls.SimplePagedResultsControl.controlType]

			if not controls:
				break

			# Set the cookie for more results
			page_control.cookie = controls[0].cookie

			# There are no more results
			if not page_control.cookie:
				break

		# Log time it took to perform the query
		logging.debug("Query took %.2fms (%s page(s))" % ((time.time() - t) * 1000.0, pages))

		return results

	def _count(self, query):
		res = self._query(query, attrlist=["dn"])

		return len(res)

	def _search(self, query, attrlist=None, limit=0):
		accounts = []
		for dn, attrs in self._query(query, attrlist=["dn"], limit=limit):
			account = self.get_by_dn(dn)
			accounts.append(account)

		return accounts

	def _get_attrs(self, dn):
		"""
			Fetches all attributes for the given distinguished name
		"""
		results = self._query("(objectClass=*)", search_base=dn, limit=1,
			attrlist=("*", "createTimestamp", "modifyTimestamp"))

		for dn, attrs in results:
			return attrs

	def get_by_dn(self, dn):
		attrs = self._get_attrs(dn)

		return Account(self.backend, dn, attrs)

	@staticmethod
	def _format_date(t):
		return t.strftime("%Y%m%d%H%M%SZ")

	def get_recently_registered(self, limit=None):
		# Check the last two weeks
		t = datetime.datetime.utcnow() - datetime.timedelta(days=14)

		# Fetch all accounts created after t
		accounts = self.get_created_after(t)

		# Order by creation date and put latest first
		accounts.sort(key=lambda a: a.created_at, reverse=True)

		# Cap at the limit
		if accounts and limit:
			accounts = accounts[:limit]

		return accounts

	def get_created_after(self, ts):
		return self._search("(&(objectClass=person)(createTimestamp>=%s))" % self._format_date(ts))

	def count_created_after(self, ts):
		return self._count("(&(objectClass=person)(createTimestamp>=%s))" % self._format_date(ts))

	def search(self, query):
		# Try finding an exact match
		account = self._search_one(
			"(&"
				"(objectClass=person)"
				"(|"
					"(uid=%s)"
					"(mail=%s)"
					"(mailAlternateAddress=%s)"
				")"
			")" % (query, query, query))
		if account:
			return [account]

		# Otherwise search for a substring match
		accounts = self._search(
			"(&"
				"(objectClass=person)"
				"(|"
					"(cn=*%s*)"
					"(uid=*%s*)"
					"(displayName=*%s*)"
					"(mail=*%s*)"
				")"
			")" % (query, query, query, query))

		return sorted(accounts)

	def _search_one(self, query):
		results = self._search(query, limit=1)

		for result in results:
			return result

	def uid_is_valid(self, uid):
		# https://unix.stackexchange.com/questions/157426/what-is-the-regex-to-validate-linux-users
		m = re.match(r"^[a-z_][a-z0-9_-]{3,31}$", uid)
		if m:
			return True

		return False

	def uid_exists(self, uid):
		if self.get_by_uid(uid):
			return True

		res = self.db.get("SELECT 1 FROM account_activations \
			WHERE uid = %s AND expires_at > NOW()", uid)

		if res:
			return True

		# Account with uid does not exist, yet
		return False

	def mail_is_valid(self, mail):
		username, delim, domain = mail.partition("@")

		# There must be an @ and a domain part
		if not domain:
			return False

		# The domain cannot end on a dot
		if domain.endswith("."):
			return False

		# The domain should at least have one dot to fully qualified
		if not "." in domain:
			return False

		# Looks like a valid email address
		return True

	def mail_is_blacklisted(self, mail):
		username, delim, domain = mail.partition("@")

		if domain:
			return self.domain_is_blacklisted(domain)

	def domain_is_blacklisted(self, domain):
		res = self.db.get("SELECT TRUE AS found FROM blacklisted_domains \
			WHERE domain = %s OR %s LIKE '%%.' || domain", domain, domain)

		if res and res.found:
			return True

		return False

	def get_by_uid(self, uid):
		return self._search_one("(&(objectClass=person)(uid=%s))" % uid)

	def get_by_mail(self, mail):
		return self._search_one("(&(objectClass=inetOrgPerson)(mail=%s))" % mail)

	def find_account(self, s):
		account = self.get_by_uid(s)
		if account:
			return account

		return self.get_by_mail(s)

	def get_by_sip_id(self, sip_id):
		if not sip_id:
			return

		return self._search_one(
			"(|(&(objectClass=sipUser)(sipAuthenticationUser=%s))(&(objectClass=sipRoutingObject)(sipLocalAddress=%s)))" \
			% (sip_id, sip_id))

	def get_by_phone_number(self, number):
		if not number:
			return

		return self._search_one(
			"(&(objectClass=inetOrgPerson)(|(sipAuthenticationUser=%s)(telephoneNumber=%s)(homePhone=%s)(mobile=%s)))" \
			% (number, number, number, number))

	@property
	def pending_registrations(self):
		res = self.db.get("SELECT COUNT(*) AS c FROM account_activations")

		return res.c or 0

	def auth(self, username, password):
		# Find account
		account = self.backend.accounts.find_account(username)

		# Check credentials
		if account and account.check_password(password):
			return account

	# Join

	def join(self, uid, email, first_name, last_name, country_code=None):
		# Convert all uids to lowercase
		uid = uid.lower()

		# Check if UID is valid
		if not self.uid_is_valid(uid):
			raise ValueError("UID is invalid: %s" % uid)

		# Check if UID is unique
		if self.uid_exists(uid):
			raise ValueError("UID exists: %s" % uid)

		# Check if the email address is valid
		if not self.mail_is_valid(email):
			raise ValueError("Email is invalid: %s" % email)

		# Check if the email address is blacklisted
		if self.mail_is_blacklisted(email):
			raise ValueError("Email is blacklisted: %s" % email)

		# Generate a random activation code
		activation_code = util.random_string(36)

		# Create an entry in our database until the user
		# has activated the account
		self.db.execute("INSERT INTO account_activations(uid, activation_code, \
			email, first_name, last_name, country_code) VALUES(%s, %s, %s, %s, %s, %s)",
			uid, activation_code, email, first_name, last_name, country_code)

		# Send an account activation email
		self.backend.messages.send_template("auth/messages/join",
			priority=100, uid=uid, activation_code=activation_code, email=email,
			first_name=first_name, last_name=last_name)

	def activate(self, uid, activation_code):
		res = self.db.get("DELETE FROM account_activations \
			WHERE uid = %s AND activation_code = %s AND expires_at > NOW() \
			RETURNING *", uid, activation_code)

		# Return nothing when account was not found
		if not res:
			return

		# Return the account if it has already been created
		account = self.get_by_uid(uid)
		if account:
			return account

		# Create a new account on the LDAP database
		account = self.create(uid, res.email,
			first_name=res.first_name, last_name=res.last_name,
			country_code=res.country_code)

		# Non-EU users do not need to consent to promo emails
		if account.country_code and not account.country_code in countries.EU_COUNTRIES:
			account.consents_to_promotional_emails = True

		# Send email about account registration
		self.backend.messages.send_template("people/messages/new-account",
			account=account)

		# Launch drip campaigns
		for campaign in ("signup", "christmas"):
			self.backend.campaigns.launch(campaign, account)

		return account

	def create(self, uid, email, first_name, last_name, country_code=None):
		cn = "%s %s" % (first_name, last_name)

		# Account Parameters
		account = {
			"objectClass"  : [b"top", b"person", b"inetOrgPerson"],
			"mail"         : email.encode(),

			# Name
			"cn"           : cn.encode(),
			"sn"           : last_name.encode(),
			"givenName"    : first_name.encode(),
		}

		logging.info("Creating new account: %s: %s" % (uid, account))

		# Create DN
		dn = "uid=%s,ou=People,dc=ipfire,dc=org" % uid

		# Create account on LDAP
		self.accounts._authenticate()
		self.ldap.add_s(dn, ldap.modlist.addModlist(account))

		# Fetch the account
		account = self.get_by_dn(dn)

		# Optionally set country code
		if country_code:
			account.country_code = country_code

		# Return account
		return account

	# Session stuff

	def create_session(self, account, host):
		session_id = util.random_string(64)

		res = self.db.get("INSERT INTO sessions(host, uid, session_id) VALUES(%s, %s, %s) \
			RETURNING session_id, time_expires", host, account.uid, session_id)

		# Session could not be created
		if not res:
			return None, None

		logging.info("Created session %s for %s which expires %s" \
			% (res.session_id, account, res.time_expires))
		return res.session_id, res.time_expires

	def destroy_session(self, session_id, host):
		logging.info("Destroying session %s" % session_id)

		self.db.execute("DELETE FROM sessions \
			WHERE session_id = %s AND host = %s", session_id, host)

	def get_by_session(self, session_id, host):
		logging.debug("Looking up session %s" % session_id)

		res = self.db.get("SELECT uid FROM sessions WHERE session_id = %s \
			AND host = %s AND NOW() BETWEEN time_created AND time_expires",
			session_id, host)

		# Session does not exist or has expired
		if not res:
			return

		# Update the session expiration time
		self.db.execute("UPDATE sessions SET time_expires = NOW() + INTERVAL '14 days' \
			WHERE session_id = %s AND host = %s", session_id, host)

		return self.get_by_uid(res.uid)

	def cleanup(self):
		# Cleanup expired sessions
		self.db.execute("DELETE FROM sessions WHERE time_expires <= NOW()")

		# Cleanup expired account activations
		self.db.execute("DELETE FROM account_activations WHERE expires_at <= NOW()")

		# Cleanup expired account password resets
		self.db.execute("DELETE FROM account_password_resets WHERE expires_at <= NOW()")

	async def _delete(self, *args, **kwargs):
		"""
			Deletes given users
		"""
		# Who is deleting?
		who = self.get_by_uid("ms")

		for uid in args:
			account = self.get_by_uid(uid)

			# Delete the account
			with self.db.transaction():
				await account.delete(who)

	# Discourse

	def decode_discourse_payload(self, payload, signature):
		# Check signature
		calculated_signature = self.sign_discourse_payload(payload)

		if not hmac.compare_digest(signature, calculated_signature):
			raise ValueError("Invalid signature: %s" % signature)

		# Decode the query string
		qs = base64.b64decode(payload).decode()

		# Parse the query string
		data = {}
		for key, val in urllib.parse.parse_qsl(qs):
			data[key] = val

		return data

	def encode_discourse_payload(self, **args):
		# Encode the arguments into an URL-formatted string
		qs = urllib.parse.urlencode(args).encode()

		# Encode into base64
		return base64.b64encode(qs).decode()

	def sign_discourse_payload(self, payload, secret=None):
		if secret is None:
			secret = self.settings.get("discourse_sso_secret")

		# Calculate a HMAC using SHA256
		h = hmac.new(secret.encode(),
			msg=payload.encode(), digestmod="sha256")

		return h.hexdigest()

	@property
	def countries(self):
		ret = {}

		for country in iso3166.countries:
			count = self._count("(&(objectClass=person)(c=%s))" % country.alpha2)

			if count:
				ret[country] = count

		return ret

	async def get_all_emails(self):
		# Returns all email addresses
		for dn, attrs in self._query("(objectClass=person)", attrlist=("mail",)):
			mails = attrs.get("mail", None)
			if not mails:
				continue

			for mail in mails:
				print(mail.decode())


class Account(LDAPObject):
	def __str__(self):
		if self.nickname:
			return self.nickname

		return self.name

	def __repr__(self):
		return "<%s %s>" % (self.__class__.__name__, self.dn)

	def __lt__(self, other):
		if isinstance(other, self.__class__):
			return self.name < other.name

		return NotImplemented

	@property
	def kerberos_principal_dn(self):
		return "krbPrincipalName=%s@IPFIRE.ORG,cn=IPFIRE.ORG,cn=krb5,dc=ipfire,dc=org" % self.uid

	@lazy_property
	def kerberos_attributes(self):
		res = self.backend.accounts._query(
			"(&(objectClass=krbPrincipal)(krbPrincipalName=%s@IPFIRE.ORG))" % self.uid,
			attrlist=[
				"krbLastSuccessfulAuth",
				"krbLastPasswordChange",
				"krbLastFailedAuth",
				"krbLoginFailedCount",
			],
			limit=1,
			search_base="cn=krb5,%s" % self.backend.accounts.search_base)

		for dn, attrs in res:
			return { key : attrs[key][0] for key in attrs }

		return {}

	@property
	def last_successful_authentication(self):
		try:
			s = self.kerberos_attributes["krbLastSuccessfulAuth"]
		except KeyError:
			return None

		return self._parse_date(s)

	@property
	def last_failed_authentication(self):
		try:
			s = self.kerberos_attributes["krbLastFailedAuth"]
		except KeyError:
			return None

		return self._parse_date(s)

	@property
	def failed_login_count(self):
		try:
			count = self.kerberos_attributes["krbLoginFailedCount"].decode()
		except KeyError:
			return 0

		try:
			return int(count)
		except ValueError:
			return 0

	def passwd(self, password):
		"""
			Sets a new password
		"""
		# The new password must have a score of 3 or better
		quality = self.check_password_quality(password)
		if quality["score"] < 3:
			raise ValueError("Password too weak")

		self.accounts._authenticate()
		self.ldap.passwd_s(self.dn, None, password)

	def check_password(self, password):
		"""
			Bind to the server with given credentials and return
			true if password is corrent and false if not.

			Raises exceptions from the server on any other errors.
		"""
		if not password:
			return

		logging.debug("Checking credentials for %s" % self.dn)

		# Set keytab to use
		os.environ["KRB5_KTNAME"] = "/etc/ipfire.org/www.keytab"

		# Check the credentials against the Kerberos database
		try:
			kerberos.checkPassword(self.uid, password, "www/%s" % FQDN, "IPFIRE.ORG")

		# Catch any authentication errors
		except kerberos.BasicAuthError as e:
			logging.debug("Could not authenticate %s: %s" % (self.uid, e))

			return False

		# Otherwise return True
		else:
			logging.info("Successfully authenticated %s" % self)

			return True

	def check_password_quality(self, password):
		"""
			Passwords are passed through zxcvbn to make sure
			that they are strong enough.
		"""
		return zxcvbn.zxcvbn(password, user_inputs=(
			self.first_name, self.last_name,
		))

	def request_password_reset(self, address=None):
		reset_code = util.random_string(64)

		self.db.execute("INSERT INTO account_password_resets(uid, reset_code, address) \
			VALUES(%s, %s, %s)", self.uid, reset_code, address)

		# Send a password reset email
		self.backend.messages.send_template("auth/messages/password-reset",
			priority=100, account=self, reset_code=reset_code)

	def reset_password(self, reset_code, new_password):
		# Delete the reset token
		res = self.db.query("DELETE FROM account_password_resets \
			WHERE uid = %s AND reset_code = %s AND expires_at >= NOW() \
			RETURNING *", self.uid, reset_code)

		# The reset code was invalid
		if not res:
			raise ValueError("Invalid password reset token for %s: %s" % (self, reset_code))

		# Perform password change
		return self.passwd(new_password)

	def is_admin(self):
		return self.is_member_of_group("sudo")

	def is_staff(self):
		return self.is_member_of_group("staff")

	def is_moderator(self):
		return self.is_member_of_group("moderators")

	def has_shell(self):
		return "posixAccount" in self.classes

	def has_mail(self):
		return "postfixMailUser" in self.classes

	def has_sip(self):
		return "sipUser" in self.classes or "sipRoutingObject" in self.classes

	def is_blog_author(self):
		return self.is_member_of_group("blog-authors")

	def is_lwl(self):
		return self.is_member_of_group("lwl-staff")

	def can_be_managed_by(self, account):
		"""
			Returns True if account is allowed to manage this account
		"""
		# Admins can manage all accounts
		if account.is_admin():
			return True

		# Users can manage themselves
		return self == account

	@property
	def classes(self):
		return self._get_strings("objectClass")

	@property
	def uid(self):
		return self._get_string("uid")

	@property
	def name(self):
		return self._get_string("cn")

	# Delete

	async def delete(self, user):
		"""
			Deletes this user
		"""
		# Check if this user can be deleted
		if not self.can_be_deleted_by(user):
			raise RuntimeError("Cannot delete user %s" % self)

		logging.info("Deleting user %s" % self)

		async with asyncio.TaskGroup() as tasks:
			t = datetime.datetime.now()

			# Disable this account on Bugzilla
			tasks.create_task(
				self._disable_on_bugzilla("Deleted by %s, %s" % (user, t)),
			)

			# XXX Delete on Discourse

		# Delete on LDAP
		self._delete()

	def can_be_deleted_by(self, user):
		"""
			Return True if the user can be deleted by user
		"""
		# Check permissions
		if not self.can_be_managed_by(user):
			return False

		# Cannot delete shell users
		if self.has_shell():
			return False

		# Looks okay
		return True

	def _delete(self):
		"""
			Deletes this object from LDAP
		"""
		# Delete the Kerberos Principal
		self._delete_dn(self.kerberos_principal_dn)

		# Delete this object
		self._delete_dn(self.dn)

	# Nickname

	def get_nickname(self):
		return self._get_string("displayName")

	def set_nickname(self, nickname):
		self._set_string("displayName", nickname)

	nickname = property(get_nickname, set_nickname)

	# First Name

	def get_first_name(self):
		return self._get_string("givenName")

	def set_first_name(self, first_name):
		self._set_string("givenName", first_name)

		# Update Common Name
		self._set_string("cn", "%s %s" % (first_name, self.last_name))

	first_name = property(get_first_name, set_first_name)

	# Last Name

	def get_last_name(self):
		return self._get_string("sn")

	def set_last_name(self, last_name):
		self._set_string("sn", last_name)

		# Update Common Name
		self._set_string("cn", "%s %s" % (self.first_name, last_name))

	last_name = property(get_last_name, set_last_name)

	@lazy_property
	def groups(self):
		return self.backend.groups._get_groups("(| \
			(&(objectClass=groupOfNames)(member=%s)) \
			(&(objectClass=posixGroup)(memberUid=%s)) \
		)" % (self.dn, self.uid))

	def is_member_of_group(self, gid):
		"""
			Returns True if this account is a member of this group
		"""
		return gid in (g.gid for g in self.groups)

	# Created/Modified at

	@property
	def created_at(self):
		return self._get_timestamp("createTimestamp")

	@property
	def modified_at(self):
		return self._get_timestamp("modifyTimestamp")

	# Address

	@property
	def address(self):
		address = []

		if self.street:
			address += self.street.splitlines()

		if self.postal_code and self.city:
			if self.country_code in ("AT", "DE"):
				address.append("%s %s" % (self.postal_code, self.city))
			else:
				address.append("%s, %s" % (self.city, self.postal_code))
		else:
			address.append(self.city or self.postal_code)

		if self.country_name:
			address.append(self.country_name)

		return [line for line in address if line]

	def get_street(self):
		return self._get_string("street") or self._get_string("homePostalAddress")

	def set_street(self, street):
		self._set_string("street", street)

	street = property(get_street, set_street)

	def get_city(self):
		return self._get_string("l") or ""

	def set_city(self, city):
		self._set_string("l", city)

	city = property(get_city, set_city)

	def get_postal_code(self):
		return self._get_string("postalCode") or ""

	def set_postal_code(self, postal_code):
		self._set_string("postalCode", postal_code)

	postal_code = property(get_postal_code, set_postal_code)

	def get_state(self):
		return self._get_string("st")

	def set_state(self, state):
		self._set_string("st", state)

	state = property(get_state, set_state)

	def get_country_code(self):
		return self._get_string("c")

	def set_country_code(self, country_code):
		self._set_string("c", country_code)

	country_code = property(get_country_code, set_country_code)

	@property
	def country_name(self):
		if self.country_code:
			return self.backend.get_country_name(self.country_code)

	@property
	def initials(self):
		initials = []

		# If a nickname is set, only use the nickname
		if self.nickname and len(self.nickname) >= 2:
			for m in re.findall(r"(\w+)", self.nickname):
				initials.append(m[0])

			# If we only detected one character, we will use the first two
			if len(initials) < 2:
				initials = [self.nickname[0], self.nickname[1]]

		# Otherwise use the first and last name
		else:
			if self.first_name:
				initials.append(self.first_name[0])

			if self.last_name:
				initials.append(self.last_name[0])

		# Truncate to two initials
		initials = initials[:2]

		return [i.upper() for i in initials]

	# Email

	@property
	def email(self):
		return self._get_string("mail")

	@property
	def email_to(self):
		return "%s <%s>" % (self, self.email)

	@lazy_property
	def alternate_email_addresses(self):
		addresses = self._get_strings("mailAlternateAddress")

		return sorted(addresses)

	# Mail Routing Address

	def get_mail_routing_address(self):
		return self._get_string("mailRoutingAddress", None)

	def set_mail_routing_address(self, address):
		self._set_string("mailRoutingAddress", address or None)

	mail_routing_address = property(get_mail_routing_address, set_mail_routing_address)

	@property
	def sip_id(self):
		if "sipUser" in self.classes:
			return self._get_string("sipAuthenticationUser")

		if "sipRoutingObject" in self.classes:
			return self._get_string("sipLocalAddress")

	@property
	def sip_password(self):
		return self._get_string("sipPassword")

	@staticmethod
	def _generate_sip_password():
		return util.random_string(8)

	@property
	def sip_url(self):
		return "%s@ipfire.org" % self.sip_id

	def uses_sip_forwarding(self):
		if self.sip_routing_address:
			return True

		return False

	# SIP Routing

	def get_sip_routing_address(self):
		if "sipRoutingObject" in self.classes:
			return self._get_string("sipRoutingAddress")

	def set_sip_routing_address(self, address):
		if not address:
			address = None

		# Don't do anything if nothing has changed
		if self.get_sip_routing_address() == address:
			return

		if address:
			# This is no longer a SIP user any more
			try:
				self._modify([
					(ldap.MOD_DELETE, "objectClass", b"sipUser"),
					(ldap.MOD_DELETE, "sipAuthenticationUser", None),
					(ldap.MOD_DELETE, "sipPassword", None),
				])
			except ldap.NO_SUCH_ATTRIBUTE:
				pass

			# Set new routing object
			try:
				self._modify([
					(ldap.MOD_ADD, "objectClass", b"sipRoutingObject"),
					(ldap.MOD_ADD, "sipLocalAddress", self.sip_id.encode()),
					(ldap.MOD_ADD, "sipRoutingAddress", address.encode()),
				])

			# If this is a change, we cannot add this again
			except ldap.TYPE_OR_VALUE_EXISTS:
				self._set_string("sipRoutingAddress", address)
		else:
			try:
				self._modify([
					(ldap.MOD_DELETE, "objectClass", b"sipRoutingObject"),
					(ldap.MOD_DELETE, "sipLocalAddress", None),
					(ldap.MOD_DELETE, "sipRoutingAddress", None),
				])
			except ldap.NO_SUCH_ATTRIBUTE:
				pass

			self._modify([
				(ldap.MOD_ADD, "objectClass", b"sipUser"),
				(ldap.MOD_ADD, "sipAuthenticationUser", self.sip_id.encode()),
				(ldap.MOD_ADD, "sipPassword", self._generate_sip_password().encode()),
			])

		# XXX Cache is invalid here

	sip_routing_address = property(get_sip_routing_address, set_sip_routing_address)

	# SIP Registrations

	async def get_sip_registrations(self):
		if not self.has_sip():
			return []

		return await self.backend.asterisk.get_registrations(self.sip_id)

	# SIP Channels

	async def get_sip_channels(self):
		if not self.has_sip():
			return []

		return await self.backend.asterisk.get_sip_channels(self.sip_id)

	# Phone Numbers

	@lazy_property
	def phone_number(self):
		"""
			Returns the IPFire phone number
		"""
		if self.sip_id:
			return phonenumbers.parse("+4923636035%s" % self.sip_id)

	@lazy_property
	def fax_number(self):
		if self.sip_id:
			return phonenumbers.parse("+49236360359%s" % self.sip_id)

	def get_phone_numbers(self):
		ret = []

		for field in ("telephoneNumber", "homePhone", "mobile"):
			for number in self._get_phone_numbers(field):
				ret.append(number)

		return ret

	def set_phone_numbers(self, phone_numbers):
		# Sort phone numbers by landline and mobile
		_landline_numbers = []
		_mobile_numbers = []

		for number in phone_numbers:
			try:
				number = phonenumbers.parse(number, None)
			except phonenumbers.phonenumberutil.NumberParseException:
				continue

			# Convert to string (in E.164 format)
			s = phonenumbers.format_number(number, phonenumbers.PhoneNumberFormat.E164)

			# Separate mobile numbers
			if phonenumbers.number_type(number) == phonenumbers.PhoneNumberType.MOBILE:
				_mobile_numbers.append(s)
			else:
				_landline_numbers.append(s)

		# Save
		self._set_strings("telephoneNumber", _landline_numbers)
		self._set_strings("mobile", _mobile_numbers)

	phone_numbers = property(get_phone_numbers, set_phone_numbers)

	@property
	def _all_telephone_numbers(self):
		ret = [ self.sip_id, ]

		if self.phone_number:
			s = phonenumbers.format_number(self.phone_number, phonenumbers.PhoneNumberFormat.E164)
			ret.append(s)

		for number in self.phone_numbers:
			s = phonenumbers.format_number(number, phonenumbers.PhoneNumberFormat.E164)
			ret.append(s)

		return ret

	# Description

	def get_description(self):
		return self._get_string("description")

	def set_description(self, description):
		self._set_string("description", description)

	description = property(get_description, set_description)

	# Avatar

	@lazy_property
	def avatar_hash(self):
		# Fetch the timestamp (or fall back to the last LDAP change)
		t = self._fetch_avatar_timestamp() or self.modified_at

		# Create the payload
		payload = "%s-%s" % (self.uid, t)

		# Compute a hash over the payload
		h = hashlib.new("blake2b", payload.encode())

		return h.hexdigest()[:7]

	def avatar_url(self, size=None, absolute=False):
		# This cannot be async because we are calling it from the template engine
		url = "/users/%s.jpg?h=%s" % (self.uid, self.avatar_hash)

		# Return an absolute URL
		if absolute:
			url = urllib.parse.urljoin("https://www.ipfire.org", url)

		if size:
			url += "&size=%s" % size

		return url

	async def get_avatar(self, size=None, format=None):
		# Check the PostgreSQL database
		photo = self._fetch_avatar()

		# Fall back to LDAP
		if not photo:
			photo = self._get_bytes("jpegPhoto")

		# Exit if no avatar is available
		if not photo:
			return

		# Return the raw image if no size was requested
		if size is None:
			return photo

		# Compose the cache key
		cache_key = "accounts:%s:avatar:%s:%s:%s" \
			% (self.uid, self.avatar_hash, format or "N/A", size)

		# Try to fetch the data from the cache
		async with await self.backend.cache.pipeline() as p:
			# Fetch the key
			await p.get(cache_key)

			# Reset the TTL
			await p.expire(cache_key, 86400)

			# Execute the pipeline
			avatar, _ = await p.execute()

		# Return the cached value (if any)
		if avatar:
			return avatar

		# Generate a new thumbnail
		avatar = util.generate_thumbnail(photo, size, square=True, format=format)

		# Save to cache for 24h
		await self.backend.cache.set(cache_key, avatar, 86400)

		return avatar

	def _fetch_avatar(self):
		"""
			Fetches the original avatar blob as being uploaded by the user
		"""
		res = self.db.get("""
			SELECT
				blob
			FROM
				account_avatars
			WHERE
				uid = %s
			AND
				deleted_at IS NULL
			""", self.uid,
		)

		if res:
			return res.blob

	def _fetch_avatar_timestamp(self):
		res = self.db.get("""
			SELECT
				created_at
			FROM
				account_avatars
			WHERE
				uid = %s
			AND
				deleted_at IS NULL
			""", self.uid,
		)

		if res:
			return res.created_at

	async def upload_avatar(self, avatar):
		# Remove all previous avatars
		self.db.execute("""
			UPDATE
				account_avatars
			SET
				deleted_at = CURRENT_TIMESTAMP
			WHERE
				uid = %s
			AND
				deleted_at IS NULL
			""", self.uid,
		)

		# Store the new avatar in the database
		self.db.execute("""
			INSERT INTO
				account_avatars
			(
				uid,
				blob
			)
			VALUES
			(
				%s, %s
			)
			""", self.uid, avatar,
		)

		# Remove anything in the LDAP database
		photo = self._get_bytes("jpegPhoto")
		if photo:
			self._delete("jpegPhoto", [photo])

	# Consent to promotional emails

	def get_consents_to_promotional_emails(self):
		return self.is_member_of_group("promotional-consent")

	def set_contents_to_promotional_emails(self, value):
		group = self.backend.groups.get_by_gid("promotional-consent")
		assert group, "Could not find group: promotional-consent"

		if value is True:
			group.add_member(self)
		else:
			group.del_member(self)

	consents_to_promotional_emails = property(
		get_consents_to_promotional_emails,
		set_contents_to_promotional_emails,
	)

	# Bugzilla

	async def _disable_on_bugzilla(self, text=None):
		"""
			Disables the user on Bugzilla
		"""
		user = await self.backend.bugzilla.get_user(self.email)

		# Do nothing if the user does not exist
		if not user:
			return

		# Disable the user
		await user.disable(text)

	# Mailman

	async def get_lists(self):
		return await self.backend.lists.get_subscribed_lists(self)


class Groups(Object):
	hidden_groups = (
		"cn=LDAP Read Only,ou=Group,dc=ipfire,dc=org",
		"cn=LDAP Read Write,ou=Group,dc=ipfire,dc=org",

		# Everyone is a member of people
		"cn=people,ou=Group,dc=ipfire,dc=org",
	)

	@property
	def search_base(self):
		return "ou=Group,%s" % self.backend.accounts.search_base

	def _query(self, *args, **kwargs):
		kwargs.update({
			"search_base" : self.backend.groups.search_base,
		})

		return self.backend.accounts._query(*args, **kwargs)

	def __iter__(self):
		groups = self.get_all()

		return iter(groups)

	def _get_groups(self, query, **kwargs):
		res = self._query(query, **kwargs)

		groups = []
		for dn, attrs in res:
			# Skip any hidden groups
			if dn in self.hidden_groups:
				continue

			g = Group(self.backend, dn, attrs)
			groups.append(g)

		return sorted(groups)

	def _get_group(self, query, **kwargs):
		kwargs.update({
			"limit" : 1,
		})

		groups = self._get_groups(query, **kwargs)
		if groups:
			return groups[0]

	def get_all(self):
		return self._get_groups(
			"(|(objectClass=posixGroup)(objectClass=groupOfNames))",
		)

	def get_by_gid(self, gid):
		return self._get_group(
			"(&(|(objectClass=posixGroup)(objectClass=groupOfNames))(cn=%s))" % gid,
		)


class Group(LDAPObject):
	def __repr__(self):
		if self.description:
			return "<%s %s (%s)>" % (
				self.__class__.__name__,
				self.gid,
				self.description,
			)

		return "<%s %s>" % (self.__class__.__name__, self.gid)

	def __str__(self):
		return self.description or self.gid

	def __lt__(self, other):
		if isinstance(other, self.__class__):
			return (self.description or self.gid) < (other.description or other.gid)

		return NotImplemented

	def __bool__(self):
		return True

	def __len__(self):
		"""
			Returns the number of members in this group
		"""
		l = 0

		for attr in ("member", "memberUid"):
			a = self.attributes.get(attr, None)
			if a:
				l += len(a)

		return l

	def __iter__(self):
		return iter(self.members)

	@property
	def gid(self):
		return self._get_string("cn")

	@property
	def description(self):
		return self._get_string("description")

	@property
	def email(self):
		return self._get_string("mail")

	@lazy_property
	def members(self):
		members = []

		# Get all members by DN
		for dn in self._get_strings("member"):
			member = self.backend.accounts.get_by_dn(dn)
			if member:
				members.append(member)

		# Get all members by UID
		for uid in self._get_strings("memberUid"):
			member = self.backend.accounts.get_by_uid(uid)
			if member:
				members.append(member)

		return sorted(members)

	def add_member(self, account):
		"""
			Adds a member to this group
		"""
		# Do nothing if this user is already in the group
		if account.is_member_of_group(self.gid):
			return

		if "posixGroup" in self.objectclasses:
			self._add_string("memberUid", account.uid)
		else:
			self._add_string("member", account.dn)

		# Append to cached list of members
		self.members.append(account)
		self.members.sort()

	def del_member(self, account):
		"""
			Removes a member from a group
		"""
		# Do nothing if this user is not in the group
		if not account.is_member_of_group(self.gid):
			return

		if "posixGroup" in self.objectclasses:
			self._delete_string("memberUid", account.uid)
		else:
			self._delete_string("member", account.dn)


if __name__ == "__main__":
	a = Accounts()

	print(a.list())
