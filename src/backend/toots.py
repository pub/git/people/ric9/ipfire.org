#!/usr/bin/python3

import datetime
import logging
import mastodon

from .misc import Object

class Toots(Object):
	async def toot(self):
		"""
			Sends a random promotional toot
		"""
		# Do not toot when there was a blog post
		if self.backend.blog.has_had_recent_activity(hours=24):
			logging.debug("Won't toot because the blog has had activity")
			return

		# Select a toot
		toot = self._get_random_toot()
		if not toot:
			logging.warning("Could not find anything to toot")
			return

		# Toot the toot!
		with self.db.transaction():
			self._toot(toot)

	def _get_random_toot(self):
		res = self.db.get(
			"WITH candidate_toots AS (SELECT id, \
				(CURRENT_TIMESTAMP - COALESCE(last_tooted_at, '1970-01-01')) * RANDOM() AS age \
				FROM toots \
				WHERE (last_tooted_at IS NULL OR last_tooted_at <= CURRENT_TIMESTAMP - INTERVAL '1 month') \
			) \
			SELECT toots.* FROM candidate_toots \
				LEFT JOIN toots ON candidate_toots.id = toots.id \
				ORDER BY age DESC LIMIT 1")

		return res

	def _toot(self, toot):
		logging.debug("Posting: %s" % toot.message)

		# Update database status
		self.db.execute("UPDATE toots \
			SET last_tooted_at = CURRENT_TIMESTAMP, total_toots = total_toots + 1 \
			WHERE id = %s", toot.id)

		# Connect to Mastodon
		conn = mastodon.Mastodon(
			client_id=self.settings.get("mastodon-client-key"),
			client_secret=self.settings.get("mastodon-client-secret"),
			access_token=self.settings.get("mastodon-access-token"),
			api_base_url="https://social.ipfire.org",
		)

		# Toot!
		conn.toot(toot.message)
