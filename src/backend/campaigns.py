#!/usr/bin/python3

import logging

from .decorators import *
from .misc import Object

class Campaigns(Object):
	async def launch_manually(self, name, uid):
		account = self.backend.accounts.get_by_uid(uid)
		if account:
			self.launch(name, account)

	def launch(self, name, account):
		logging.debug("Launching all campaigns for %s" % account)

		# Update old timestamps first
		self.db.execute("UPDATE campaign_templates \
			SET launch_at = launch_at + repeat_after \
			WHERE (launch_at IS NOT NULL AND launch_at <= CURRENT_TIMESTAMP) \
				AND repeat_after IS NOT NULL")

		# Launch all campaigns
		self.db.execute("INSERT INTO campaign_emails(account_uid, template, \
			launch_at, repeat_after, promotional) \
			SELECT %s, template, COALESCE(launch_at, CURRENT_TIMESTAMP + launch_after), \
				repeat_after, promotional FROM campaign_templates WHERE name = %s",
			account.uid, name)

	def _get_campaign_emails(self, query, *args):
		res = self.db.query(query, *args)

		for row in res:
			yield CampaignEmail(self.backend, row.id, data=row)

	async def run(self):
		with self.db.transaction():
			emails = self._get_campaign_emails("SELECT * FROM campaign_emails \
				WHERE launch_at <= CURRENT_TIMESTAMP ORDER BY launch_at")

			# Send them all
			for email in emails:
				email.send()


class CampaignEmail(Object):
	def init(self, id, data=None):
		self.id   = id
		self.data = data

	@lazy_property
	def account(self):
		return self.backend.accounts.get_by_uid(self.data.account_uid)

	@property
	def template(self):
		return self.data.template

	@property
	def repeat_after(self):
		return self.data.repeat_after

	@property
	def promotional(self):
		return self.data.promotional

	@property
	def should_be_sent(self):
		# Send all non-promotional emails
		if not self.promotional:
			return True

		# For promotional emails, send them if we have consent
		return self.account.consents_to_promotional_emails

	def send(self):
		# Delete if the account does not exist any more
		if not self.account:
			return self._delete()

		logging.debug("Sending %s to %s" % (self.template, self.account))

		# Generate the email
		if self.should_be_sent:
			self.backend.messages.send_template(self.template, account=self.account)

		# Update this email for the next launch
		self._update()

	def _update(self):
		# If this email repeats, we will update the timestamp
		if self.repeat_after:
			self.db.execute("UPDATE campaign_emails \
				SET launch_at = launch_at + repeat_after \
				WHERE id = %s", self.id)

		# Otherwise we will delete it
		else:
			self._delete()

	def _delete(self):
		"""
			Deletes this email
		"""
		self.db.execute("DELETE FROM campaign_emails WHERE id = %s", self.id)
