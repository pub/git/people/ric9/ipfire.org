#!/usr/bin/python

import configparser
import io
import location
import logging
import ssl
import tempfile
import tornado.httpclient

from . import accounts
from . import asterisk
from . import analytics
from . import blog
from . import bugzilla
from . import cache
from . import campaigns
from . import database
from . import fireinfo
from . import httpclient
from . import iuse
from . import lists
from . import messages
from . import mirrors
from . import netboot
from . import nopaste
from . import ratelimit
from . import releases
from . import resolver
from . import settings
from . import toots
from . import util
from . import wiki
from . import zeiterfassung
from .decorators import *

DEFAULT_CONFIG = io.StringIO("""
[global]
debug = false
environment = testing

data_dir      =
static_dir    = %(data_dir)s/static
templates_dir = %(data_dir)s/templates
""")

# Setup logging
log = logging.getLogger(__name__)

class Backend(object):
	version = 0

	def __init__(self, configfile, debug=False):
		# Read configuration file.
		self.config = self.read_config(configfile)

		# Enable debug logging if configured
		self.debug = debug or self.config.getboolean("global", "debug")

		# Setup database.
		self.setup_database()

		# Create HTTPClient
		self.http_client = httpclient.HTTPClient(self)

		# Initialize the cache
		self.cache = cache.Cache(self)

		# Initialize settings first
		self.settings = settings.Settings(self)

		# Initialize backend modules.
		self.accounts = accounts.Accounts(self)
		self.analytics = analytics.Analytics(self)
		self.bugzilla = bugzilla.Bugzilla(self)
		self.fireinfo = fireinfo.Fireinfo(self)
		self.iuse = iuse.IUse(self)
		self.mirrors = mirrors.Mirrors(self)
		self.netboot = netboot.NetBoot(self)
		self.nopaste = nopaste.Nopaste(self)
		self.releases = releases.Releases(self)

		self.blog = blog.Blog(self)
		self.wiki = wiki.Wiki(self)
		self.zeiterfassung = zeiterfassung.ZeiterfassungClient(self)

	def read_config(self, configfile):
		cp = configparser.ConfigParser()

		# Initialize configuration with some sensible defaults
		cp.readfp(DEFAULT_CONFIG)

		# Parse file
		cp.read(configfile)

		return cp

	@property
	def environment(self):
		"""
			Returns whether this is running in "production" or "testing"
		"""
		return self.config.get("global", "environment")

	def setup_database(self):
		"""
			Sets up the database connection.
		"""
		credentials = {
			"host"     : self.config.get("database", "server"),
			"database" : self.config.get("database", "database"),
			"user"     : self.config.get("database", "username"),
			"password" : self.config.get("database", "password"),
		}

		self.db = database.Connection(self, **credentials)

	@lazy_property
	def ssl_context(self):
		# Create SSL context
		context = ssl.create_default_context()

		# Fetch client certificate
		certificate = self.settings.get("client-certificate", None)
		key         = self.settings.get("client-key", None)

		# Apply client certificate
		if certificate and key:
			with tempfile.NamedTemporaryFile(mode="w") as f_cert:
				f_cert.write(certificate)
				f_cert.flush()

				with tempfile.NamedTemporaryFile(mode="w") as f_key:
					f_key.write(key)
					f_key.flush()

					context.load_cert_chain(f_cert.name, f_key.name)

		return context

	async def load_certificate(self, certfile, keyfile):
		with self.db.transaction():
			# Load certificate
			with open(certfile) as f:
				self.settings.set("client-certificate", f.read())

			# Load key file
			with open(keyfile) as f:
				self.settings.set("client-key", f.read())

	async def run_task(self, task, *args, **kwargs):
		tasks = {
			"accounts:delete"     : self.accounts._delete,
			"announce-blog-posts" : self.blog.announce,
			"check-mirrors"       : self.mirrors.check_all,
			"cleanup"             : self.cleanup,
			"get-all-emails"      : self.accounts.get_all_emails,
			"launch-campaigns"    : self.campaigns.launch_manually,
			"load-certificate"    : self.load_certificate,
			"run-campaigns"       : self.campaigns.run,
			"scan-files"          : self.releases.scan_files,
			"send-message"        : self.messages.send_cli,
			"send-all-messages"   : self.messages.queue.send_all,
			"test-ldap"           : self.accounts.test_ldap,
			"toot"                : self.toots.toot,
			"update-blog-feeds"   : self.blog.update_feeds,
		}

		# Get the task from the list of all tasks
		func = tasks.get(task, None)
		if not func:
			raise ValueError("Unknown task: %s" % task)

		# Check if we are running in production
		if not self.environment == "production":
			log.warning("Refusing to run task '%s' in '%s' environment" % (task, self.environment))
			return

		# Run the task
		r = await func(*args, **kwargs)

		# If any error code has been returned,
		# we will end the program
		if r:
			raise SystemExit(r)

	@lazy_property
	def asterisk(self):
		return asterisk.Asterisk(self)

	@lazy_property
	def campaigns(self):
		return campaigns.Campaigns(self)

	@lazy_property
	def groups(self):
		return accounts.Groups(self)

	@lazy_property
	def lists(self):
		return lists.Lists(self)

	@lazy_property
	def messages(self):
		return messages.Messages(self)

	@lazy_property
	def location(self):
		return location.Database("/var/lib/location/database.db")

	def get_country_name(self, country_code):
		country = self.location.get_country(country_code)

		if country:
			return country.name

	@lazy_property
	def ratelimiter(self):
		return ratelimit.RateLimiter(self)

	@lazy_property
	def resolver(self):
		return resolver.Resolver(tries=2, timeout=2, domains=[])

	@lazy_property
	def toots(self):
		return toots.Toots(self)

	async def cleanup(self):
		# Cleanup message queue
		with self.db.transaction():
			self.messages.queue.cleanup()

		# Cleanup in accounts
		with self.db.transaction():
			self.accounts.cleanup()

		# Cleanup nopasts
		with self.db.transaction():
			self.nopaste.cleanup()
