#!/usr/bin/python

import hashlib
import logging
import os
import re
import urllib.parse

from . import database
from .misc import Object
from .decorators import *

class File(Object):
	def __init__(self, backend, release, id, data=None):
		Object.__init__(self, backend)

		self.id = id
		self._release = release

		# get all data from database
		self.__data = data

	def __eq__(self, other):
		if isinstance(other, self.__class__):
			return self.id == otherid

	def __lt__(self, other):
		if isinstance(other, self.__class__):
			return self.prio < other.prio

	@property
	def data(self):
		if self.__data is None:
			self.__data = self.db.get("SELECT * FROM files WHERE id = %s", self.id)
			assert self.__data

		return self.__data

	@property
	def release(self):
		if not self._release:
			release_id = self.data.get("releases")
			self._release = Release(release_id)

		return self._release

	@property
	def type(self):
		filename = self.filename

		if filename.endswith(".iso"):
			return "iso"

		elif "xen" in filename:
			if "downloader" in filename:
				return "xen-downloader"

			return "xen"

		elif "sources" in filename:
			return "source"

		elif "usb-fdd" in filename:
			return "usbfdd"

		elif "usb-hdd" in filename:
			return "usbhdd"

		elif "armv5tel" in filename and "scon" in filename:
			return "armv5tel-scon"

		elif "armv5tel" in filename:
			return "armv5tel"

		elif "scon" in filename:
			return "alix"

		elif filename.endswith(".img.gz") or filename.endswith(".img.xz"):
			return "flash"

		else:
			return "unknown"

	@property
	def url(self):
		return urllib.parse.urljoin("https://downloads.ipfire.org", self.filename)

	@property
	def desc(self):
		_ = lambda x: x

		descriptions = {
			"armv5tel"	: _("Flash Image"),
			"armv5tel-scon"	: _("Flash Image with serial console"),
			"iso"		: _("ISO Image"),
			"flash"		: _("Flash Image"),
			"alix"		: _("Flash Image with serial console"),
			"usbfdd"	: _("USB FDD Image"),
			"usbhdd"	: _("USB HDD Image"),
			"xen"		: _("Pre-generated Xen Image"),
			"xen-downloader": _("Xen-Image Generator"),
		}

		try:
			return descriptions[self.type]
		except KeyError:
			return _("Unknown image type")

	@property
	def prio(self):
		priorities = {
			"iso"		: 10,
			"flash"		: 40,
			"alix"		: 41,
			"usbfdd"	: 31,
			"usbhdd"	: 30,
			"armv5tel"	: 40,
			"armv5tel-scon"	: 41,
			"xen"		: 50,
			"xen-downloader": 51,
		}

		try:
			return priorities[self.type]
		except KeyError:
			return 999

	@property
	def sha256(self):
		return self.data.get("sha256")

	@property
	def sha1(self):
		return self.data.get("sha1")

	@property
	def filename(self):
		return self.data.get("filename")

	@property
	def basename(self):
		return os.path.basename(self.filename)

	@property
	def size(self):
		return self.data.get("filesize")

	@property
	def arch(self):
		known_arches = ("x86_64", "aarch64", "arm", "i586")

		for arch in known_arches:
			if arch in self.basename:
				return arch

		return "N/A"


class Release(Object):
	def __init__(self, backend, id, data=None):
		Object.__init__(self, backend)
		self.id = id

		# get all data from database
		self.__data = data or self.db.get("SELECT * FROM releases WHERE id = %s", self.id)
		assert self.__data

		self.__files = []

	def __str__(self):
		return self.name

	def __repr__(self):
		return "<%s %s>" % (self.__class__.__name__, self.name)

	def __cmp__(self, other):
		return cmp(self.id, other.id)

	@property
	def arches(self):
		for arch in ("x86_64", "aarch64", "i586", "arm"):
			if arch in (f.arch for f in self.files):
				yield arch

	@property
	def primary_arches(self):
		arches = []

		# Add x86_64 when available, otherwise add i586
		for arch in ("x86_64", "i586"):
			if arch in self.arches:
				arches.append(arch)
				break

		# Add aarch64 if available
		if "aarch64" in self.arches:
			arches.append("aarch64")

		# Add ARM before 2.27 if available
		if "arm" in self.arches and self.sname < "ipfire-2.27-core159":
			arches.append("arm")

		return arches

	@property
	def secondary_arches(self):
		arches = []

		for arch in self.arches:
			if arch in self.primary_arches:
				continue

			arches.append(arch)

		return arches

	@property
	def experimental_arches(self):
		return []

	@property
	def files(self):
		if not self.__files:
			files = self.db.query("SELECT * FROM files WHERE releases = %s \
				AND NOT filename LIKE '%%.torrent'", self.id)

			self.__files = [File(self.backend, self, f.id, f) for f in files]
			self.__files.sort()

		return self.__files

	def get_files_by_arch(self, arch):
		for f in self.files:
			if f.arch == arch:
				yield f

	@property
	def name(self):
		return self.__data.name

	@property
	def slug(self):
		return self.__data.sname

	# XXX compat
	sname = slug

	@lazy_property
	def blog(self):
		if self.__data.blog_id:
			return self.backend.blog.get_by_id(self.__data.blog_id)

	@property
	def stable(self):
		return self.__data.stable

	@property
	def published(self):
		return self.__data.published

	date = published

	@property
	def path(self):
		return self.__data.path

	def get_file(self, type):
		for file in self.files:
			if file.type == type:
				return file

	def __file_hash(self, filename, algo="sha256"):
		h = hashlib.new(algo)

		with open(filename, "rb") as f:
			buf_size = 1024
			buf = f.read(buf_size)
			while buf:
				h.update(buf)
				buf = f.read(buf_size)

		return h.hexdigest()

	def scan_files(self, basepath="/pub/mirror"):
		if not self.path:
			return

		path = os.path.join(basepath, self.path)
		if not os.path.exists(path):
			return

		files = self.db.query("SELECT filename FROM files WHERE releases = %s", self.id)
		files = [f.filename for f in files]

		# Make files that do not exists not loadable.
		for filename in files:
			_filename = os.path.join(basepath, filename)
			if not os.path.exists(_filename):
				self.db.execute("UPDATE files SET loadable='N' WHERE filename = %s", filename)

		for filename in os.listdir(path):
			filename = os.path.join(path, filename)

			if os.path.isdir(filename):
				continue

			_filename = re.match(".*(releases/.*)", filename).group(1)
			if _filename in files:
				continue

			if filename.endswith(".b2") or filename.endswith(".md5"):
				continue

			logging.info("Hashing %s..." % filename)
			hash_sha256 = self.__file_hash(filename, "sha256")
			hash_sha1   = self.__file_hash(filename, "sha1")
			filesize = os.path.getsize(filename)

			self.db.execute("INSERT INTO files(releases, filename, filesize, \
				sha256, sha1) VALUES(%s, %s, %s, %s, %s)",
				self.id, _filename, filesize, hash_sha256, hash_sha1)

	def supports_arch(self, arch):
		return arch in ("x86_64", "i586")

	def supports_platform(self, platform):
		# Currently there is nothing else than pcbios supported
		if platform == "pcbios":
			return True

		return False

	def is_netboot_capable(self):
		return self.path and "ipfire-2.x" in self.path

	def netboot_kernel_url(self, arch, platform):
		assert self.supports_arch(arch)
		assert self.supports_platform(platform)

		if self.sname >= "ipfire-2.19-core100":
			return "http://boot.ipfire.org/%s/images/%s/vmlinuz" % (self.path, arch)

		return "http://boot.ipfire.org/%s/images/vmlinuz" % self.path

	def netboot_initrd_url(self, arch, platform):
		assert self.supports_arch(arch)
		assert self.supports_platform(platform)

		if self.sname >= "ipfire-2.19-core100":
			return "http://boot.ipfire.org/%s/images/%s/instroot" % (self.path, arch)

		return "http://boot.ipfire.org/%s/images/instroot" % self.path

	def netboot_args(self, arch, platform):
		return ""

	@property
	def post(self):
		if self.__data.blog_id:
			return self.backend.blog.get_by_id(self.__data.blog_id)

	# Fireinfo Stuff

	def get_usage(self, when=None):
		name = self.sname.replace("ipfire-", "IPFire ").replace("-", " - ")

		# Get penetration from fireinfo
		releases = self.backend.fireinfo.get_releases_map(when=when)

		return releases.get(name, 0)


class Releases(Object):
	def _get_release(self, query, *args):
		res = self.db.get(query, *args)

		if res:
			return Release(self.backend, res.id, data=res)

	def _get_releases(self, query, *args):
		res = self.db.query(query, *args)

		for row in res:
			yield Release(self.backend, row.id, data=row)

	def __iter__(self):
		releases = self._get_releases("SELECT * FROM releases \
			ORDER BY published DESC NULLS FIRST")

		return iter(releases)

	def get_by_id(self, id):
		ret = self.db.get("SELECT * FROM releases WHERE id = %s", id)

		if ret:
			return Release(self.backend, ret.id, data=ret)

	def get_by_sname(self, sname):
		ret = self.db.get("SELECT * FROM releases WHERE sname = %s", sname)

		if ret:
			return Release(self.backend, ret.id, data=ret)

	def get_by_news_id(self, news_id):
		ret = self.db.get("SELECT * FROM releases WHERE news_id = %s", news_id)

		if ret:
			return Release(self.backend, ret.id, data=ret)

	def get_latest(self, stable=True):
		ret = self.db.get("SELECT * FROM releases WHERE published IS NOT NULL AND published <= NOW() \
			AND stable = %s ORDER BY published DESC LIMIT 1", stable)

		if ret:
			return Release(self.backend, ret.id, data=ret)

	def get_releases_older_than(self, release, limit=None):
		return self._get_releases("SELECT * FROM releases \
			WHERE published IS NOT NULL AND published < %s \
				ORDER BY published DESC LIMIT %s", release.published, limit)

	def get_latest_unstable(self):
		ret = self.db.get("SELECT * FROM releases r1 \
			WHERE r1.published IS NOT NULL AND r1.published <= NOW() \
			AND stable = %s AND NOT EXISTS ( \
				SELECT * FROM releases r2 WHERE r2.stable = %s AND \
					r2.published IS NOT NULL AND r2.published >= r1.published \
				) ORDER BY r1.published DESC LIMIT 1", False, True)

		if ret:
			return Release(self.backend, ret.id, data=ret)

	def get_stable(self):
		query = self.db.query("SELECT * FROM releases \
			WHERE published IS NOT NULL AND published <= NOW() AND stable = TRUE \
			ORDER BY published DESC")

		releases = []
		for row in query:
			release = Release(self.backend, row.id, data=row)
			releases.append(release)

		return releases

	def get_unstable(self):
		query = self.db.query("SELECT * FROM releases \
			WHERE published IS NOT NULL AND published <= NOW() AND stable = FALSE \
			ORDER BY published DESC")

		releases = []
		for row in query:
			release = Release(self.backend, row.id, data=row)
			releases.append(release)

		return releases

	def get_all(self):
		query = self.db.query("SELECT * FROM releases \
			WHERE published IS NOT NULL AND published <= NOW() \
			ORDER BY published DESC")

		releases = []
		for row in query:
			release = Release(self.backend, row.id, data=row)
			releases.append(release)

		return releases

	def _get_all(self):
		query = self.db.query("SELECT * FROM releases ORDER BY published DESC")

		releases = []
		for row in query:
			release = Release(self.backend, row.id, data=row)
			releases.append(release)

		return releases

	async def scan_files(self, basepath="/pub/mirror"):
		for release in self:
			logging.debug("Scanning %s..." % release)

			with self.db.transaction():
				release.scan_files(basepath=basepath)

	def get_file_by_filename(self, filename):
		ret = self.db.get("SELECT * FROM files WHERE filename = %s", filename)

		if ret:
			return File(self.backend, None, ret.id, data=ret)
