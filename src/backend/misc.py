#!/usr/bin/python

import psycopg.adapt

class Object(object):
	def __init__(self, backend, *args, **kwargs):
		self.backend = backend

		self.init(*args, **kwargs)

	def init(self, *args, **kwargs):
		"""
			Function for custom initialization.
		"""
		pass

	@property
	def db(self):
		return self.backend.db

	@property
	def accounts(self):
		return self.backend.accounts

	@property
	def downloads(self):
		return self.backend.downloads

	@property
	def fireinfo(self):
		return self.backend.fireinfo

	@property
	def iuse(self):
		return self.backend.iuse

	@property
	def settings(self):
		return self.backend.settings


# SQL Integration

class ObjectDumper(psycopg.adapt.Dumper):
	def dump(self, obj):
		# Return the ID (as bytes)
		return bytes("%s" % obj.id, "utf-8")
