#!/usr/bin/python

import io
import logging
import os.path

from PIL import Image, ImageDraw, ImageFont, PngImagePlugin

from .misc import Object
from .decorators import *

image_types = []

class IUse(Object):
	def get_imagetype(self, id):
		id = int(id)

		for image_type in image_types:
			if image_type.id == id:
				return image_type


class ImageObject(Object):
	_filename = None

	def init(self, request, profile):
		self.request = request
		self.profile = profile

		# Create new image
		if self.filename and os.path.exists(self.filename):
			with Image.open(self.filename) as image:
				self._image = image.convert("RGBA")
		else:
			self._image = Image.new("RGBA", (100, 100))

		self.render()

	def to_string(self):
		with io.BytesIO() as f:
			self._image.save(f, "PNG", optimize=True)

			return f.getvalue()

	@lazy_property
	def font(self):
		fontfile = os.path.join(
			self.request.application.settings.get("static_path", ""),
			"fonts/Prompt-Regular.ttf"
		)

		return ImageFont.truetype(fontfile, 15, encoding="unic")

	@lazy_property
	def draw(self):
		return ImageDraw.Draw(self._image)

	def draw_text(self, pos, text, **kwargs):
		return self.draw.text(pos, text, font=self.font, **kwargs)

	@property
	def filename(self):
		if not self._filename:
			return

		return os.path.join(
			self.request.application.settings.get("template_path", ""),
			"fireinfo", self._filename
		)

	@property
	def locale(self):
		return self.request.locale


class Image1(ImageObject):
	id = 0

	default_size = 500, 50
	_filename = "i-use-1.png"

	def render(self):
		_ = self.locale.translate

		line1 = [_("%s on %s") % (self.profile.system.release, self.profile.processor.arch),]
		line2 = []

		# Show the hypervisor vendor for virtual machines
		if self.profile.system.is_virtual():
			if self.profile.hypervisor:
				line2.append(_("Virtualised on %s") % self.profile.hypervisor)
			else:
				line2.append(_("Running in a virtualised environment"))

		# Otherwise show some general hardware information of the machine
		else:
			if self.profile.processor:
				line2.append(self.profile.processor.friendly_string)

			line2.append(self.profile.system.friendly_memory)

		self.draw_text((225, 5), " | ".join(line1))
		self.draw_text((225, 23), "%s" % " - ".join(line2))


image_types.append(Image1)
