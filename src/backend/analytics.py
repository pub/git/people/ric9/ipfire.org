#!/usr/bin/python3

import datetime
import json
import urllib.parse

from . import misc
from .decorators import *

INVALID_REFERRERS = (
	# Broken schema
	"://",

	# Localhost
	"http://localhost",
	"https://localhost",
	"http://127.0.0.1",
	"https://127.0.0.1",
)

class Analytics(misc.Object):
	def log_unique_visit(self, address, referrer, country_code=None, user_agent=None,
			host=None, uri=None, source=None, medium=None, campaign=None, content=None,
			term=None, q=None):
		"""
			Logs a unique visit to this a page
		"""
		asn, query_args, bot = None, None, False

		if referrer:
			# Parse referrer
			url = urllib.parse.urlparse(referrer)

			# Remove everything after ? and #
			referrer = "%s://%s%s" % (url.scheme, url.netloc, url.path)

			# Drop anything that isn't valid
			for invalid_referrer in INVALID_REFERRERS:
				if referrer.startswith(invalid_referrer):
					referrer = None
					break

		# Fetch the ASN
		if address:
			asn = address.asn

		# Strip URI
		if uri:
			uri, _, query_args = uri.partition("?")

		# Parse query arguments
		if query_args:
			query_args = urllib.parse.parse_qs(query_args)

		# Mark bots
		if user_agent:
			bot = "bot" in user_agent.lower()

		# Split q
		if q:
			q = q.split()

		self.db.execute("""
			INSERT INTO
				analytics_unique_visits
			(
				host,
				uri,
				query_args,
				country_code,
				asn,
				referrer,
				user_agent,
				q,
				bot,
				source,
				medium,
				campaign,
				content,
				term
			)
			VALUES
			(
				%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s
			)
			""",
			host, uri, json.dumps(query_args or {}), country_code, asn, referrer or "",
			user_agent, q, bot, source or "", medium or "", campaign or "", content or "",
			term or "",
		)

	def get_total_page_views(self, host, since=None):
		# Make since an absolute timestamp
		if since and isinstance(since, datetime.timedelta):
			since = datetime.datetime.utcnow() - since

		if since:
			res = self.db.get("""
				SELECT
					COUNT(*) AS c
				FROM
					analytics_unique_visits
				WHERE
					host = %s
				AND
					created_at >= %s
				""", host, since,
			)
		else:
			res = self.db.get("""
				SELECT
					COUNT(*) AS c
				FROM
					analytics_unique_visits
				WHERE
					host = %s
				""", host,
			)

		if res and res.c:
			return res.c

		return 0

	def get_page_views(self, host, uri, since=None):
		# Make since an absolute timestamp
		if since and isinstance(since, datetime.timedelta):
			since = datetime.datetime.utcnow() - since

		if since:
			res = self.db.get("""
				SELECT
					COUNT(*) AS c
				FROM
					analytics_unique_visits
				WHERE
					host = %s
				AND
					uri = %s
				AND
					created_at >= %s
				""", host, uri, since,
			)
		else:
			res = self.db.get("""
				SELECT
					COUNT(*) AS c
				FROM
					analytics_unique_visits
				WHERE
					host = %s
				AND
					uri = %s
				""", host, uri,
			)

		if res and res.c:
			return res.c

		return 0

	# Popular Pages

	def get_most_popular_docs_pages(self, host, since=None, offset=None, limit=None):
		# Make since an absolute timestamp
		if since and isinstance(since, datetime.timedelta):
			since = datetime.datetime.utcnow() - since

		pages = self.backend.wiki._get_pages("""
			SELECT
				wiki.*,
				COUNT(*) AS _c
			FROM
				wiki_current
			LEFT JOIN
				wiki ON wiki_current.id = wiki.id
			LEFT JOIN
				analytics_unique_visits
					ON (CASE WHEN wiki.page = '/' THEN '/docs'
						ELSE '/docs' || wiki.page END) = analytics_unique_visits.uri
			WHERE
				host = %s
			AND
				uri LIKE '/docs%%'
			GROUP BY
				wiki.id
			ORDER BY
				_c DESC
			LIMIT
				%s
			OFFSET
				%s
			""", host, limit, offset,
		)

		return list(pages)

	# Search

	def get_search_queries(self, host, uri, limit=None):
		res = self.db.query("""
			SELECT
				q,
				COUNT(*) AS c
			FROM
				analytics_unique_visits
			WHERE
				host = %s
			AND
				uri = %s
			AND
				q IS NOT NULL
			GROUP BY
				q
			LIMIT
				%s
			""", host, uri, limit,
		)

		return { " ".join(row.q) : row.c for row in res }
