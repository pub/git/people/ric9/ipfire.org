#!/usr/bin/python

import iso3166

ZONES = {
	# Europe
	"EU": ["AD", "AL", "AT", "AX", "BA", "BE", "BG", "BY", "CH", "CZ", "DE", "DK", "EE",
			"ES", "EU", "FI", "FO", "FR", "FX", "GB", "GG", "GI", "GR", "HR", "HU", "IE",
			"IM", "IS", "IT", "JE", "LI", "LT", "LU", "LV", "MC", "MD", "ME", "MK", "MT",
			"NL", "NO", "PL", "PT", "RO", "RS", "RU", "SE", "SI", "SJ", "SK", "SM", "TR",
			"UA", "VA"],

	# Asia
	"AS": ["AE", "AF", "AM", "AP", "AZ", "BD", "BH", "BN", "BT", "CC", "CN", "CX", "CY",
			"GE", "HK", "ID", "IL", "IN", "IO", "IQ", "IR", "JO", "JP", "KG", "KH", "KP",
			"KR", "KW", "KZ", "LA", "LB", "LK", "MM", "MN", "MO", "MV", "MY", "NP", "OM",
			"PH", "PK", "PS", "QA", "SA", "SG", "SY", "TH", "TJ", "TL", "TM", "TW", "UZ",
			"VN", "YE"],

	# North America
	"NA": ["AG", "AI", "AN", "AW", "BB", "BL", "BM", "BS", "BZ", "CA", "CR", "CU", "DM",
			"DO", "GD", "GL", "GP", "GT", "HN", "HT", "JM", "KN", "KY", "LC", "MF", "MQ",
			"MS", "MX", "NI", "PA", "PM", "PR", "SV", "TC", "TT", "US", "VC", "VG", "VI"],

	# South America
	"SA": ["AR", "BO", "BR", "CL", "CO", "EC", "FK", "GF", "GY", "PE", "PY", "SR", "UY", "VE"],

	# Africa
	"AF": ["AO", "BF", "BI", "BJ", "BW", "CD", "CF", "CG", "CI", "CM", "CV", "DJ", "DZ",
			"EG", "EH", "ER", "ET", "GA", "GH", "GM", "GN", "GQ", "GW", "KE", "KM", "LR",
			"LS", "LY", "MA", "MG", "ML", "MR", "MU", "MW", "MZ", "NA", "NE", "NG", "RE",
			"RW", "SC", "SD", "SH", "SL", "SN", "SO", "ST", "SZ", "TD", "TG", "TN", "TZ",
			"UG", "YT", "ZA", "ZM", "ZW"],

	# Antartica
	"AN": ["AQ", "BV", "GS", "HM", "TF"],

	# Oceania
	"OC": ["AS", "AU", "CK", "FJ", "FM", "GU", "KI", "MH", "MP", "NC", "NF", "NR", "NU", "NZ",
			"PF", "PG", "PN", "PW", "SB", "TK", "TO", "TV", "UM", "VU", "WF", "WS"],
}

EU_COUNTRIES = (
	"BE",
	"BG",
	"CZ",
	"DK",
	"DE",
	"EE",
	"IE",
	"EL",
	"ES",
	"FR",
	"FR",
	"GB",
	"HR",
	"IT",
	"CY",
	"LV",
	"LT",
	"LU",
	"HU",
	"MT",
	"NL",
	"AT",
	"PL",
	"PT",
	"RO",
	"SI",
	"SK",
	"FI",
	"SE",
)

def get_name(code):
	try:
		return iso3166.countries_by_alpha2[code].name
	except KeyError:
		return code

def get_all(locale=None):
	return sorted(iso3166.countries, key=lambda c: c.name)

def get_zone(country_code):
	for zone in ZONES:
		if country_code in ZONES[zone]:
			return zone

def get_in_zone(zone):
	try:
		return ZONES[zone]
	except KeyError:
		return []
